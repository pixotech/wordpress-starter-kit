# Asset enqueueing

Assets can be loaded site wide or on a per-template basis. Most javascript should be enqueued by the Twig
template that needs it. This allows us to only load the JS that is needed on a given page.

## Organization

All top level JS files in the theme's `js/` directory and stylus files in the theme's `scss` directory will be
**registered by WordPress automatically**. Supporting JS files can be stored in the `modules/` directory and imported as needed.

```text
js/
├── main.js (import modules here)
├── modules/
│  ├── main-navigation.js
│  └── small-main-navigation.js
├── vimeo.js
└── youtube.js

scss/
├── main.scss (Ensure your files are imported here)
├── base/
│  └── base.scss
├── global/
│  ├── layout.scss
│  └── type.scss
├── mixins/
│  ├── fluid.scss
│  └── visually-hidden.scss
├── _variables/
│  ├── color.scss
│  ├── layout.scss
│  └── typography.scss
└── reset.scss
```

## Register in Webpack

Each top-level JS and stylus file will be automatically registered in `webpack.config.js` and will be compiled to
`web/app/themes/custom/dist/js/` and `web/app/themes/custom/dist/css`, respectively.

## Enqueue in Twig

Enqueueing JS in Twig is done with the `enqueue_script` function for js and the `enqueue_style` function for css.
The argument to this function is the filename without an extension.

```twig
{{ enqueue_script('main') }}
{{ enqueue_style('main') }}
```

## Manually add script to Fractal preview template

Since Fractal doesn't know about Wordpress we have to manually load those scripts and styles in the preview template. This is done
in the `_preview.twig` file.

```html
<!-- patterns/_preview.twig -->
<!DOCTYPE html>
<html>
<head>
    <title>{{ _target.label }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width"/>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body class="page" {% block bodyTag%}{% endblock %} >

{{ yield }}

{% block afterYield %}
{% endblock %}

<!-- Scripts -->
<script src="/js/main.js"></script>
<script src="/js/youtube.js"></script>
<script src="/js/vimeo.js"></script>
<!-- End Scripts -->

</body>
</html>
```
