# Stylelint

The front-end build process uses [Stylelint](https://stylelint.io/) to guide and enforce best practices and consistent code style. When using the `npm run watch` command, Stylelint will output warnings when best practices or misconfigured CSS is detected. It will even fix certain things for you automatically.

For more information read the [documentation](https://stylelint.io/) and review the [.stylelintrc](../web/app/themes/custom/.stylelintrc) configuration file.

A few important rules:
* Ruleset ordering
* Alphabetize rule declarations
* Use REM units over pixels