# Docker

Docker is used for the local environment for this project. These settings are based on the [docker4wordpress](https://wodby.com/docs/1.0/stacks/wordpress/local/#domains) project. It has its own documentation and is a good resource for updating this starter kit when needed.

## Notable docker-compose.yml settings

### maridb

This volume maps the wordpress mysql database from the docker container into the repository (at ./.mysql) to keep the data persistent even if the docker container is removed.

```
        volumes:
            - ./.mysql:/var/lib/mysql

```
