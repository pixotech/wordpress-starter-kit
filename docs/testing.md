# Cypress

## Setting up your local for testing
There is a env-example-test file in the root of the project. Copy this file and rename it to .env.test.
This file will be used by Cypress to run the tests. You will need to add license keys for ACF and Gravity Forms.

There are also separate docker containers for php, nginx, and mariadb. Run `docker-compose up -d --build` to start the containers.

Run `./bin/setup-test.sh` and then `./bin/sync-local-test.sh` to get your local test environment setup working

## Running the tests
Make sure you have an entry for start-kit-test.localhost in your /etc/hosts file.

Run `npm run cypress:interactive` to open the Cypress test runner in interactive mode.
From there you can run the tests in the browser or run them headless.

## Keeping the test data up-to-date
The sync script imports a file called testing.xml that has all of the test data
(using the built-in WP exporter, it's all the content from setting up test data when finishing. Images won't import correctly but that
shouldn't affect the tests)

## Writing tests
TODO: Add documentation on how to write tests
