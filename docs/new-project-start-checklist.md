# Starting a new Project

Here is a list of tasks necessary for starting a new project based off of the Starter Kit.

## Update the Starter Kit

The Starter Kit should be updated before use. Updates should be done and committed to the Starter Kit repository so they persist for the next project.

1. Update Node to the latest stable version. Change required version in `package.json`. (see [build tool documentation](/docs/build-tooling.md))
1. Update npm packages. (see [build tool documentation](/docs/build-tooling.md))
2. Update composer packages (includes WP Core updates). (see [composer documentation](/docs/composer.md))
3. Find the highest PHP version compatible with WordPress to use in next step. (check compatibility on [wp documentation](https://make.wordpress.org/core/handbook/references/php-compatibility-and-wordpress-versions/))
4. Update docker containers to the highest appropriate version. (see [docker4wordpress env file](https://github.com/wodby/docker4wordpress/blob/master/.env) for reference to tag images in the `docker-compose.yml` e.g. line 5)
5. Review documentation and setup process to ensure it still works as expected. Update anything as needed.
6. Commit all changes to the original starter kit repo.

## Start a new repository

1. Clone the starter kit repo
2. Delete the `.git` directory
3. Run `git init`
4. Create a new empty repo for the new project
5. Commit the content to the new git repo: run `git add .` and `git commit -m "Initial commit."`
6. Associate the new repo origin to the local project: run `git remote add origin <new-repo-git-uri>` (find uri on bitbucket by clicking 'Clone' and using the 'git@bitbucket.org...')
7. Push it! Run `git push -u --force origin develop` (make sure this branch name matches the main branch on the remote new repo)

## Update README & Project metadata

1. Replace the top level README.md with the [README_template.md](/README_template.md) and fill in the placeholder text with appropriate project titles.
2. Update the project settings in the [env-example](/docker/env-example) with the appropriate names for the new project. This should be done before running any setup so the .env file reflects the changes.
3. Update the project title in the [fractal config file](/web/app/themes/custom/fractal.config.js) on line 25.
4. Remove any unneeded code as necessary.
