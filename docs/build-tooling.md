# Build tools

## NPM

NPM is a Node package manager. This starter kit uses Node packages just for building the website.

### Update npm packages

All of this must be done in the theme folder:
`cd web/app/themes/custom`

List outdated packages:
`npm outdated`

Update all packages (minor versions only):
`npm update`

Update a single package (minor versions only):
`npm update <package>`

To update a package to a new major release, update the version requirements in the [package.json](/web/app/themes/custom/package.json), then run `npm update`.

### Update Node

When updating Node, make sure to update the required version in the package.json file. The version is enforced when running `npm install`, so it must match the desired Node version.

Example:
```
    "engines" : {
        "node" : ">=22.13.0"
    },
```
