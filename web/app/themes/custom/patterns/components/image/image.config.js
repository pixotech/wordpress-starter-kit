module.exports = {
    handle: "image-component",
    context: {
        component: {
            type: 'image',
            image: {
                src: 'holder.js/800x600?auto=yes&random=yes',
                alt: 'placeholder',
            },
        }
    },
    variants: [
        {
            name: 'Image with caption',
            context: {
                component: {
                    image: {
                        src: 'holder.js/800x600?auto=yes&random=yes',
                        alt: 'placeholder',
                        caption: 'Caption of image',
                    },
                }
            },
        }
    ]
};
