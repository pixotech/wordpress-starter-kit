module.exports = {
    title: 'Button',
    context: {
        button: {
            text: 'Take action',
            url: '#',
            isDisabled: false,
        }
    },
    variants: [
        {
            name: 'Disabled button',
            context: {
                button: {
                    isButton: true,
                    isDisabled: true,
                }
            }
        },        {
            name: 'Variant button',
            context: {
                variant: true
            }
        },
        {
            name: 'Submit button - full width',
            context: {
                button: {
                    isButton: true,
                    type: 'submit',
                    width: 'full'
                }
            }
        },
    ]
}
