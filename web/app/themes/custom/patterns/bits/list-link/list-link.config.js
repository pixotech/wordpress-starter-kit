module.exports = {
    context: {
        fields: {
            linkType: 'external',
            className: '',
            linkText: 'External Link',
            external_url: 'https://google.com',
            internal_url: '',
            file_url: '',
        }
    },
    variants: [
        {
            name: 'Internal Link',
            context: {
                fields: {
                    linkType: 'internal',
                    linkText: 'Internal Link',
                    internal_url: 'https://google.com'
                }
            }
        },
        {
            name: 'File Link',
            context: {
                fields: {
                    linkType: 'file',
                    linkText: 'File Link',
                    file_url: 'https://google.com'
                }
            }
        },
    ]
}
