module.exports = {
    name: 'Color circle icon',
    context: {
        name: 'Edit',
        icon: {
            name: 'edit'
        }
    },
    variants: [
        {
            name: 'calendar',
            context: {
                icon: {
                    name: 'calendar'
                }
            }
        },
        {
            name: 'Quote',
            context: {
                icon: {
                    name: 'quote'
                }
            }
        },
        {
            name: 'Instagram',
            context: {
                icon: {
                    name: 'instagram'
                }
            }
        },
        {
            name: 'Internal link',
            context: {
                icon: {
                    name: 'internal-link'
                }
            }
        },
        {
            name: 'External Link',
            context: {
                icon: {
                    name: 'external-link'
                }
            }
        },
    ]
};
