module.exports = {
    name: 'Link with icon',
    context: {
        fields: {
            linkText: 'See more items',
            linkUrl: '#',
            icon: { name: 'calendar'}
        },
    },
    hidden: true
};
