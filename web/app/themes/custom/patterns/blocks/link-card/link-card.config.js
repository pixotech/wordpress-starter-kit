module.exports = {
    title: 'Link card',
    context: {
        fields: {
            type: 'link-card',
            showImage: true,
            image: {
                url: 'https://placekitten.com/272/352?a',
            },
            image_alt_text: 'friendly kitten',
            link_text: 'See your location',
            link_reference: '#',
            description: 'These are the benefits of a program or student organization',
        },
    },
    hidden: true,
    variants: [
        {
            name: 'No image',
            context: {
                fields: {
                    showImage: false
                },
            }
        },
    ]
};
