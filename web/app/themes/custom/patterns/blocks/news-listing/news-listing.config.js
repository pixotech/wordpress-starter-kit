
module.exports = {
    title: 'News Listing',
    context: {
        post: {
            title: 'News',
            list: [
                {
                    title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
                    permalink: 'http://localhost:3000/components/detail/news',
                    publishDate: 'November 19, 2019',
                    featuredImage: {
                        src: 'https://placekitten.com/320/228',
                        altText: 'friendly kitten',
                    }
                },
                {
                    title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
                    permalink: 'http://localhost:3000/components/detail/news',
                    publishDate: 'November 19, 2019',
                    featuredImage: {
                        src: 'https://placekitten.com/320/228',
                        altText: 'friendly kitten',
                    }
                }
            ]
        },
    },
    variants: [
        {
            name: 'No posts found',
            context: {
                post: {
                    list: []
                },
                listingError: {
                    heading: 'No news stories found',
                    message: 'Try browsing all news, or searching the site for a specific story.'
                },
                pagination: null
            }
        }
    ]
};
