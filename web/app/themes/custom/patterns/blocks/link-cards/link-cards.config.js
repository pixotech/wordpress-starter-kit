module.exports = {
    title: 'Link cards',
    context: {
        fields: {
            type: 'link-cards',
            header: 'Link cards heading',
            description: 'Link cards description',
            showDescription: true,
            innerBlocks: {
                cards: [
                    {
                        fields: {
                            type: 'link-card',
                            showImage: true,
                            image: {
                                url: 'holder.js/300x700?auto=yes&random=yes',
                            },
                            image_alt_text: 'placeholder',
                            link_text: 'See your location',
                            link_reference: '#',
                            description: 'These are the benefits of a program or student organization. This is a really long description so we can see how the link cards get styled.',
                            focalPoint: {
                                x: 50,
                                y: 50,
                            }
                        },
                    },
                    {
                        fields: {
                            type: 'link-card',
                            showImage: false,
                            image: {
                                url: 'holder.js/350x750?auto=yes&random=yes',
                            },
                            image_alt_text: 'placeholder',
                            link_text: 'See your location',
                            link_reference: '#',
                            description: 'These are the benefits of a program or student organization',
                        },
                    },
                    {
                        fields: {
                            type: 'link-card',
                            showImage: true,
                            image: {
                                url: 'holder.js/350x750?auto=yes&random=yes',
                            },
                            image_alt_text: 'placeholder',
                            link_text: 'See your location',
                            link_reference: '#',
                            description: 'These are the benefits of a program or student organization',
                            focalPoint: {
                                x: 50,
                                y: 50,
                            }
                        },
                    },
                    {
                        fields: {
                            type: 'link-card',
                            showImage: true,
                            image: {
                                url: 'holder.js/350x750?auto=yes&random=yes',
                            },
                            image_alt_text: 'placeholder',
                            link_text: 'See your location',
                            link_reference: '#',
                            description: 'These are the benefits of a program or student organization. This is a really long description so we can see how the link cards get styled.',
                            focalPoint: {
                                x: 50,
                                y: 50,
                            }
                        },
                    },
                ]
            },
        },
    },
    variants: [
        {
            name: 'No description',
            context: {
                fields: {
                    showDescription: false
                },
            }
        },
    ]
};
