module.exports = {
    context: {
        fields: {
            type: 'cta',
            leadInText: 'Interested in learning more?',
            isJumbo: false,
            linkText: 'Learn more',
            linkUrl: '#',
            showDescription: false,
            description: 'Here is more information to get your attention.',
            innerBlocks: {
                callToActionList: [
                    {
                        fields: {
                            linkText: 'Apply now',
                            linkUrl: '#',
                            icon: {name: 'edit'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'Contact us',
                            linkUrl: '#',
                            icon: {name: 'calendar'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'Upcoming events',
                            linkUrl: '#',
                            icon: {name: 'calendar'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'Find out more',
                            linkUrl: '#',
                            icon: {name: 'right-chevron'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'Read the bio',
                            linkUrl: '#',
                            icon: {name: 'star-filled'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'Ask a question',
                            linkUrl: '#',
                            icon: {name: 'quote'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'See your location',
                            linkUrl: '#',
                            icon: {name: 'cloud'}
                        }
                    },
                    {
                        fields: {
                            linkText: 'Visit the website',
                            linkUrl: '#',
                            icon: {name: 'file-link'}
                        }
                    }
                ]
            },
        }
    },
    variants: [
        {
            name: 'Long CTA text',
            context: {
                fields: {
                    leadInText: 'Interested in learning more about this interesting topic?',
                    isJumbo: false,
                    linkText: 'Learn more',
                    linkUrl: '#',
                },
            }
        },
        {
            name: 'Jumbo call to action',
            context: {
                fields: {
                    isJumbo: true,
                    showDescription: true,
                },
            }
        },
        {
            name: 'Jumbo call to action - no description',
            context: {
                fields: {
                    isJumbo: true,
                },
            }
        },
    ]
};
