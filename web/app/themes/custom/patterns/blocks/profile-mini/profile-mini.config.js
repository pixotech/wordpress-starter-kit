module.exports = {
    title: 'Profile Mini',
    context: {
        component: {
            type: 'profile-mini',
            title: 'The latest news about kittens',
            cards: [
                {
                    fullName: 'Terence J. Galbraith',
                    titles: '',
                    photo: {
                        src: 'https://placekitten.com/272/352',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'TerenceJGalbraith@jourrapide.com',
                    phoneNumber: '623-327-6582',
                    permalink: '#',
                },
                {
                    fullName: 'Wendy D. Sanchez',
                    titles: '',
                    photo: {
                        src: 'https://placekitten.com/272/352',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'WendyDSanchez@teleworm.us',
                    phoneNumber: '615-691-3250',
                    permalink: '#',
                },
                {
                    fullName: 'Antone T. Grissom',
                    titles: '',
                    photo: {
                        src: 'https://placekitten.com/272/352',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'AntoneTGrissom@rhyta.com',
                    phoneNumber: '425-502-0739',
                    permalink: '#',
                },
            ],
            callToAction: {
                text: 'See all staff',
                url: '#',
            }
        }
    },
};
