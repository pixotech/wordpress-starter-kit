module.exports = {
    title: 'Giving',
    context: {
        fields: {
            type: 'giving',
            showHeading: true,
            heading: 'Optional header for giving form',
            buttonText: 'Continue',
            innerBlocks: {
                funds: [
                    {
                        fields: {
                            name: 'College of Fine and Applied Arts Annual Fund',
                            description: 'Description of fund description of fund fund description of fund fund description of fund fund description of fund fund.',
                            fundId: '11331630',
                            givingUrl: '#?fund=11331630'
                        }
                    },
                    {
                        fields: {
                            name: 'Arts at Illinois Scholarship Fund',
                            description: '',
                            fundId: '11341209',
                            givingUrl: '#?fund=11341209'
                        }
                    }
                ]
            }
        },
    },
    variants: [
        {
            name: 'No heading',
            context: {
                fields: {
                    showHeading: false
                },
            }
        },
    ]
};
