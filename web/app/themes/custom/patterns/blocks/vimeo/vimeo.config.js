module.exports = {
    title: 'Vimeo embed',
    context: {
        fields: {
            oembed: {
                id: "860232965",
                title: "Vimeo video title",
            },
            caption: "Vimeo video caption",
        }
    },
    variants: [
        {
            name: 'No caption',
            context: {
                fields: {
                    caption: false
                }
            }
        },
        {
            name: 'No video Javascript',
            context: {
                fields: {
                    caption: "Disable Javascript in the browser to see the fallback link."
                }
            }
        }
    ]
}
