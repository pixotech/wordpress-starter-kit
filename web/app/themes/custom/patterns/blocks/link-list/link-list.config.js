module.exports = {
    preview: '@preview-on-cloud-dark',
    context: {
        componentSpacing: 'large',
        background: 'light',
        component: {
            type: 'link-list',
            lists: [
                {
                    name: 'List one name',
                    links: [
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'internal',
                            text: 'Internal link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'file',
                            text: 'File link',
                            url: 'http://www.africau.edu/images/default/sample.pdf'
                        },
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                    ]
                },
                {
                    name: 'Long text list',
                    links: [
                        {
                            type: 'external',
                            text: 'External link with an extraordinarily, unnecessarily, ridiculously, absurdly long name.',
                            url: 'https://google.com',
                        },
                        {
                            type: 'internal',
                            text: 'Internal link with an extraordinarily, unnecessarily, ridiculously, absurdly long name.',
                            url: 'https://google.com',
                        },
                        {
                            type: 'file',
                            text: 'File link with an extraordinarily, unnecessarily, ridiculously, absurdly long name.',
                            url: 'http://www.africau.edu/images/default/sample.pdf'
                        },
                    ]
                },
                {
                    name: 'List three name',
                    links: [
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'internal',
                            text: 'Internal link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'file',
                            text: 'File link',
                            url: 'http://www.africau.edu/images/default/sample.pdf'
                        },
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                    ]
                },
                {
                    name: 'List four name',
                    links: [
                        {
                            type: 'internal',
                            text: 'Internal link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'file',
                            text: 'File link',
                            url: 'http://www.africau.edu/images/default/sample.pdf'
                        },
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                        {
                            type: 'external',
                            text: 'External link',
                            url: 'https://google.com',
                        },
                    ]
                }
            ]
        }
    },
    variants: [
        {
            name: 'On A Dark charcoalLighter Background',
            preview: '@preview-on-charcoal-lighter',
            context: {
                background: 'dark'
            },
        },
    ]
};
