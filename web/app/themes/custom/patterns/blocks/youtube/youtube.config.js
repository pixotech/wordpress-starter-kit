module.exports = {
    title: 'YouTube embed',
    handle: 'youtube-embed',
    context: {
        fields: {
            oembed: {
                id: "3cynqSLcV7o",
                title: "YouTube video title",
            },
            caption: "YouTube video caption",
        }
    },
    variants: [
        {
            name: 'No caption',
            context: {
                fields: {
                    caption: false
                }
            }
        },
        {
            name: 'No video Javascript',
            context: {
                fields: {
                    caption: "Disable Javascript in the browser to see the fallback link."
                }
            }
        }
    ]
}
