module.exports = {
    title: 'Directory',
    context: {
        fields: {
            title: 'Meet our faculty',
            introText: '<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas auguae, eu vulputate magna eros.</p>',
        },
        count: 10,
        post: {
            list: [
                {
                    fullName: 'John C. Gunnels',
                    titles: '',
                    featuredImage: {
                        src: 'https://placekitten.com/272/352?a',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'JohnCGunnels@teleworm.us',
                    phoneNumber: '832-731-9009',
                    permalink: '#',
                },
                {
                    fullName: 'Terence J. Galbraith',
                    titles: '',
                    featuredImage: {
                        src: 'https://placekitten.com/272/352?b',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'TerenceJGalbraith@jourrapide.com',
                    phoneNumber: '623-327-6582',
                    permalink: '#',
                },
                {
                    fullName: 'Wendy D. Sanchez',
                    titles: '',
                    featuredImage: {
                        src: 'https://placekitten.com/272/352?c',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'WendyDSanchez@teleworm.us',
                    phoneNumber: '615-691-3250',
                    permalink: '#',
                },
                {
                    fullName: 'Antone T. Grissom',
                    titles: '',
                    featuredImage: {
                        src: 'https://placekitten.com/272/352?d',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'AntoneTGrissom@rhyta.com',
                    phoneNumber: '425-502-0739',
                    permalink: '#',
                },
                {
                    fullName: 'Angela E. Stpierre',
                    titles: '',
                    featuredImage: {
                        src: 'https://placekitten.com/272/352?e',
                        altText: 'friendly kitten',
                    },
                    emailAddress: 'AngelaEStpierre@armyspy.com',
                    phoneNumber: '706-307-8998',
                    permalink: '#',
                },
            ]
        }
    },
    variants: [
        {
            name: 'No posts found',
            context: {
                post: {
                    list: []
                },
                listingError: {
                    heading: 'No profiles found',
                    message: 'Try browsing all profiles, or searching the site for a specific person.'
                },
                pagination: null
            }
        },
    ]
};
