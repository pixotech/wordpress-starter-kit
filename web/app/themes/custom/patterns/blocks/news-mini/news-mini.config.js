
module.exports = {
    title: 'News Mini',
    context: {
        componentSpacing: 'large',
        background: 'light',
        component: {
            type: 'news-mini',
            title: 'The latest news about kittens',
            cards: [
                {
                    title: 'Etiam fermentum nibh vitae viverra laoreet',
                    permalink: 'http://localhost:3000/components/detail/news',
                    publishDate: 'November 19, 2019',
                    featuredImage: {
                        src: 'https://placekitten.com/320/228',
                        altText: 'friendly kitten',
                    }
                },
                {
                    title: 'Nulla ipsum libero, aliquam et dictum sed, porttitor eget lorem',
                    permalink: 'http://localhost:3000/components/detail/news',
                    publishDate: 'November 19, 2019',
                    featuredImage: {
                        src: 'https://placekitten.com/320/228',
                        altText: 'friendly kitten',
                    }
                },
                {
                    title: 'Aliquam condimentum velit sed nisl euismod, sit amet interdum erat vestibulum',
                    permalink: 'http://localhost:3000/components/detail/news',
                    publishDate: 'November 19, 2019',
                    featuredImage: {
                        src: 'https://placekitten.com/320/228',
                        altText: 'friendly kitten',
                    }
                },
                {
                    title: 'Integer eget suscipit erat',
                    permalink: 'http://localhost:3000/components/detail/news',
                    publishDate: 'November 19, 2019',
                    featuredImage: {
                        src: 'https://placekitten.com/320/228',
                        altText: 'friendly kitten',
                    }
                },
            ],
            callToAction: {
                text: 'Read more news',
                url: '#',
            }
        }
    },
};
