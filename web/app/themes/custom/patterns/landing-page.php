<?php
/**
 * Title: Landing Page
 * Slug: custom/landing-page
 * Block Types: core/post-content
 * Categories: text
 * Description: For top level pages
 * Keywords: example, test
 */

?>

<!-- wp:custom/page-layout /-->
<!-- wp:custom/page-layout {"layout":"landing"} /-->
<!-- wp:custom/quote {"quoteText":"Quote text","attribution":"Person name","imageId":114} /-->
