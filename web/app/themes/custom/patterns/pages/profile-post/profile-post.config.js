module.exports = {
    title: 'Profile Page',
    status: 'ready',
    context: {
        post: {
            firstName: 'Bobby',
            lastName: 'Tables',
            photo: {
                src: 'https://picsum.photos/id/18/248/310',
                altText: 'Bobby sitting at a desk, writing sql'
            },
            titles: 'Database Administrator',
            emailAddress: 'bobby@pixotech.com',
            phoneNumber: '217.344.0444',
        },
    },
};
