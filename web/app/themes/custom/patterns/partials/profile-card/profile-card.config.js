module.exports = {
    title: 'Profile card',
    context: {
        person: {
            fullName: 'Wendy D. Sanchez',
            titles: '',
            photo: {
                src: 'https://placekitten.com/272/352',
                altText: 'friendly kitten',
            },
            emailAddress: 'WendyDSanchez@teleworm.us',
            phoneNumber: '615-691-3250',
            permalink: '#',
        },
    },
};
