module.exports = {
    handle: "image-partial",
    context: {
        image: {
            src: 'https://picsum.photos/800/650',
            altText: 'placeholder',
        },
    },
    variants: [
        {
            name: 'Image with caption',
            context: {
                image: {
                    src: 'https://picsum.photos/800/650',
                    altText: 'placeholder',
                    caption: 'Caption of image',
                },
            },
        }
    ]
};
