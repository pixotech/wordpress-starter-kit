module.exports = {
    context: {
        fields: {
            globalOptions: [
                {
                    label: "Category 1",
                    value: "1"
                },
                {
                    label: "Category 2",
                    value: "2"
                },
                {
                    label: "Category 3",
                    value: "3"
                },
                {
                    label: "Category 4",
                    value: "4",
                    selected: true
                },
            ],
            localOptions: [
                {
                    label: "Thing 1",
                    value: "1"
                },
                {
                    label: "Thing 2",
                    value: "2",
                    selected: true
                },
                {
                    label: "Thing 3",
                    value: "3"
                },
                {
                    label: "Thing 4",
                    value: "4"
                },
            ],
            submitButtonText: 'View',
            cancelButtonText: 'Clear',
        },
    },
};
