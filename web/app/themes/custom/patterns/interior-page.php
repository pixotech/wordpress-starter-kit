<?php
/**
 * Title: (Default) Interior Page
 * Slug: custom/interior-page
 * Block Types: core/post-content
 * Categories: text
 * Description: For most page content
 * Keywords: example, test
 */

?>

<!-- wp:heading {"lock":{"move":true,"remove":true}} -->
<h2>Replace these blocks with the interior page blocks(s)</h2>
<!-- /wp:heading -->
<!-- wp:image {"lock":{"move":true,"remove":true}} -->
<figure class="wp-block-image"><img alt=""/></figure>
<!-- /wp:image -->
<!-- wp:paragraph -->
<p>Hello World!</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph -->
<p>Hello World!</p>
<!-- /wp:paragraph -->

