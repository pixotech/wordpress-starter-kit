<?php
/**
 * Title: News Listing
 * Slug: custom/news-listing
 * Block Types: core/post-content
 * Categories: text
 * Description: A listing of news stories
 * Keywords: example, test
 */

?>

<!-- wp:heading -->
<h2>Replace these blocks with the block(s) for news listing</h2>
<!-- /wp:heading -->
<!-- wp:paragraph -->
<p>Hello World!</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph -->
<p>Hello World!</p>
<!-- /wp:paragraph -->
<!-- wp:image -->
<figure class="wp-block-image"><img alt=""/></figure>
<!-- /wp:image -->
