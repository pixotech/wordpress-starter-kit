---
title: Type styles
order: 3
context:
    groups:
        billboards:
            - billboard
        headlines:
            - headline1
            - headline2
            - headline3
            - headline4
            - headline5
            - headline6
        Body:
            - bodyLarge
            - body
            - bodySmall
        details:
            - detail
            - detailBold
        labels:
            - label
            - labelSmall
        linkedHeadlines:
            - linkHeadline3
            - linkHeadline4
            - linkHeadline5
            - linkHeadline6
        Links:
            - linkBodyLarge
            - linkBody
            - linkBodySmall
---

<div>
    {{#each groups}}
        <h2>{{@key}}</h2>
        <div class="style-table">
            {{#each this}}
               <div class="type-style {{this}}" data-name="{{this}}">
                   <div>Sphinx of black quartz, judge my vow.</div>
               </div>
            {{/each}}
        </div>
    {{/each}}
</div>


{{{renderSass 'type-styles.scss'}}}
<script src="/js/table.js"></script>
<link rel="stylesheet" href="https://use.typekit.net/kuc5lwk.css">
