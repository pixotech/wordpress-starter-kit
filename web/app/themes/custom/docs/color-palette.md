---
title: Color Palette
order: 2
context:
    groups:
        primary:
            - navy
            - aqua
            - coral
            - gold
        secondary:
            - aqua
            - aquaDark
        charcoal:
            - charcoalDark
            - charcoal
            - charcoalLight
            - charcoalLighter
            - charcoalLightest
        smoke:
            - smoke
            - smokeDark
            - smokeDarker
            - smokeDarkest
        black/white:
            - black
            - white
---

<div>
    {{#each groups}}
        <h2>{{@key}}</h2>
        <div class="style-table">
            {{#each this}}
               <div class="color-swatch" data-name="{{this}}">
                   <div class="color-data">
                       <div class="color-data__name">{{this}}</div>
                       <div class="color-data__hexCode">{{lookup ../colors this}}</div>
                   </div>
                   <div class="color-square background-{{this}}"></div>
               </div>
            {{/each}}
        </div>
    {{/each}}
</div>

{{{renderSass 'color-palette.scss'}}}
<script src="/js/hexValues.js"></script>
