const Encore = require("@symfony/webpack-encore");
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');
// Import the original config from the @wordpress/scripts package.
const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );

const globImporter = require('node-sass-glob-importer');


// Import the helper to find and generate the entry points in the src directory
const { getWebpackEntryPoints } = require( '@wordpress/scripts/utils/config' );
const {readdirSync} = require("node:fs");


if (!Encore.isRuntimeEnvironmentConfigured())
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || "dev");

Encore.setOutputPath("dist/")
    .setPublicPath("/app/themes/custom/dist")
    .setManifestKeyPrefix("dist/")
    .cleanupOutputBeforeBuild()
    .disableSingleRuntimeChunk()
    .enableSourceMaps(!Encore.isProduction())
    .enableBuildNotifications(true, (options) => {
        options.alwaysNotify = true;
    })
    .copyFiles({
        from: "./assets/images",
        to: "images/[path][name].[ext]",
    })
    .configureBabel((babelConfig) => {
        babelConfig.presets.push("@babel/preset-react");
    })
    .enableSassLoader(function (options) {
        options.sassOptions = {
            importer: globImporter()
        }
    })
    .enablePostCssLoader(function (options) {
        options.postcssOptions = {
            plugins: {
                autoprefixer: {
                    grid: "autoplace",
                },
                "rucksack-css": {},
            },
        };
    })
    .configureCssLoader(function (options) {
        options.url = false;
    });

const jsFileNames = readdirSync('./js').filter(fileName => fileName.endsWith('.js')).map(fileName => fileName.replace(/\.js$/, ''));
jsFileNames.forEach(fileName => {
    Encore.addEntry(`js/${fileName}`, `./js/${fileName}.js`);
});

const sassFileNames = readdirSync('./scss').filter(fileName => fileName.endsWith('.scss')).map(fileName => fileName.replace(/\.scss$/, ''));
sassFileNames.forEach(fileName => {
    Encore.addStyleEntry(`css/${fileName}`, `./scss/${fileName}.scss`);
});

    const wpEntryPoints = getWebpackEntryPoints();
    Object.keys(wpEntryPoints).forEach((entryName) => {
        Encore.addEntry(entryName, wpEntryPoints[entryName]);
    });

module.exports = Encore.getWebpackConfig();
