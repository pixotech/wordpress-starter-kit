<?php

namespace App\PostTypes;

use Rareloop\Lumberjack\Post;

class Profile extends Post
{
    /**
     * Return the key used to register the post type with WordPress
     * First parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return string
     */
    public static function getPostType()
    {
        return 'profile';
    }

    /**
     * Return the config to use to register the post type with WordPress
     * Second parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array|null
     */
    protected static function getPostTypeConfig()
    {
        return [
            'labels' => [
                'name' => __('Profile'),
                'singular_name' => __('Profile'),
                'add_new_item' => __('Add New Profile'),
            ],
            'public' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'author', 'editor', 'custom-fields'),
            'template' => [
                ['custom/profile-post', array(
                    'lock' => array(
                        'move' => true,
                        'remove' => true,
                    )
                )],
                ['custom/link-list', array(
                    'lock' => array(
                        'move' => true,
                        'remove' => true,
                    )
                )]
            ],
            'rewrite' => [
                'slug' => 'people/profiles'
            ]
        ];
    }
}
