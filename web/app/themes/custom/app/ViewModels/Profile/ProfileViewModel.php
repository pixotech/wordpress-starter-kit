<?php

namespace App\ViewModels\Profile;

use App\Utilities\TaxonomyHelper;
use App\ViewModels\ComponentViewModelFactory;
use Timber\Image;

class ProfileViewModel
{

    public static function createFromPost($post)
    {
//        dd($post);
        $model = new ProfileViewModel();

        // Contact information
//        $model->prefix = $post->profile_meta_block_prefix;
        $model->firstName = $post->profile_meta_block_first_name;
        $model->lastName = $post->profile_meta_block_last_name;
        $image = (new Image($post->profile_meta_block_image_id));
        if ($image !== false) {
            $focalPoint = wpautop($post->profile_meta_block_focal_point);

            $model->featuredImage = [
                'src' => $image->src(),
                'altText' => $image->alt(),
                'caption' => $image->caption(),
                'height' => $image->height(),
                'width' => $image->width(),
                'focalPoint' => $focalPoint,
            ];
        }
        $model->titles = $post->profile_meta_block_titles;
        $model->phoneNumber = $post->profile_meta_block_phone_number;
        $model->emailAddress = $post->profile_meta_block_email_address;

        $model->content = $post->content;

        return $model;
    }
}
