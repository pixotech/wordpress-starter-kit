<?php

namespace App\ViewModels\Profile;

class DirectoryViewModel
{
    public $list;

    public static function createFromPost($post)
    {
        $model = new DirectoryViewModel();

        $model->title = $post->title;

        return $model;
    }
}
