<?php

namespace App\ViewModels;

use Timber\Post;

class NewsListingViewModel
{
    public static function createFromPost($post)
    {
        $model = new NewsListingViewModel();

        $model->title = $post->title;

        return $model;
    }
}
