<?php

namespace App\ViewModels;

use App\Utilities\DateFormatter;
use Timber\Image;
use Timber\Post;

class NewsStoryViewModel
{
    public $title;
    public $featuredImage;
    public $summary;
    public $content;
    public $permalink;

    public static function createFromPost(Post $post)
    {
        $viewModel = new NewsStoryViewModel();
        $viewModel->permalink = $post->link;
        $viewModel->title = $post->post_title;
        $date = new \DateTime($post->post_date);
        $viewModel->date = DateFormatter::getDateStringFromDate($date);
        $viewModel->summary = wpautop($post->news_meta_block_summary);
        $image = (new Image($post->news_meta_block_image_id));
        if ($image !== false) {
            $focalPoint = wpautop($post->news_meta_block_focal_point);

            $viewModel->featuredImage = [
                'src' => $image->src(),
                'altText' => $image->alt(),
                'caption' => $image->caption(),
                'height' => $image->height(),
                'width' => $image->width(),
                'focalPoint' => $focalPoint,
            ];
        }

        $viewModel->content = $post->content;

        return $viewModel;
    }
}
