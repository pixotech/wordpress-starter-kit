<?php

namespace App\Utilities;

use Doctrine\Inflector\InflectorFactory;

class RegisterTaxonomies {
    public static function initialize($name, $postType, $hierachical = false) {
        $create_topics_nonhierarchical_taxonomy = function() use ($name, $postType, $hierachical) {
            $inflector = InflectorFactory::create()->build();

            $cleanName = str_replace('_', ' ', $name);
            strtolower($cleanName);
            $plural = $inflector->pluralize($cleanName);
            $singular = $inflector->singularize($cleanName);
            $uppercasePlural = $inflector->capitalize($plural);
            $uppercaseSingular = $inflector->capitalize($singular);

            $labels = array(
                'name' => _x( $uppercasePlural, 'taxonomy general name' ),
                'singular_name' => _x( $uppercaseSingular, 'taxonomy singular name' ),
                'search_items' =>  __( "Search {$uppercasePlural}" ),
                'popular_items' => __( "Popular {$uppercasePlural}" ),
                'all_items' => __( "All {$uppercasePlural}" ),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __( "Edit {$uppercaseSingular}" ),
                'update_item' => __( "Update {$uppercaseSingular}" ),
                'add_new_item' => __( "Add New {$uppercaseSingular}" ),
                'new_item_name' => __( "New {$uppercaseSingular} Name" ),
                'separate_items_with_commas' => __( "Separate {$plural} with commas" ),
                'add_or_remove_items' => __( "Add or remove {$plural}" ),
                'choose_from_most_used' => __( "Choose from the most used {$plural}" ),
                'menu_name' => __( $uppercasePlural ),
            );

            register_taxonomy($name, $postType, array(
                'hierarchical' => $hierachical,
                'labels' => $labels,
                'show_ui' => true,
                'show_in_rest' => true,
                'show_in_quick_edit' => false,
                'meta_box_cb' => false,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                'rewrite' => array( 'slug' => $inflector->urlize($singular) ),
            ));
        };
        add_action( 'init', $create_topics_nonhierarchical_taxonomy, 0 );
    }
}
