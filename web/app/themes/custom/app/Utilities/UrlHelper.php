<?php

namespace App\Utilities;

class UrlHelper
{
    public static function getTaxonomyFilters($request)
    {
        $searchQuery = self::getSearchQuery($request);
        $globalTaxonomyQuery = self::getGlobalTaxonomyQuery($request);
        $localTaxonomyQuery = self::getLocalTaxonomyQuery($request);

        return [
            'search' => $searchQuery,
            'globalTaxonomy' => $globalTaxonomyQuery,
            'localTaxonomy' => $localTaxonomyQuery,
        ];
    }

    public static function getSearchQuery($request)
    {
        if ($request->get != null && array_key_exists('search', $request->get) ){
            $search = $request->get['search'] ?: null;
        } else {
            $search = null;
        }

        return $search;
    }

    public static function getGlobalTaxonomyQuery($request)
    {
        if ($request->get != null && array_key_exists('global-taxonomy', $request->get) ) {
            $globalTaxonomy = $request->get['global-taxonomy'] != 'all' ? $request->get['global-taxonomy'] : null;
        } else {
            $globalTaxonomy = null;
        }

        return $globalTaxonomy;
    }

    public static function getLocalTaxonomyQuery($request)
    {
        if ($request->get != null && array_key_exists('local-taxonomy', $request->get) ) {
            $localTaxonomy = $request->get['local-taxonomy'] != 'all' ? $request->get['local-taxonomy'] : null;
        } else {
            $localTaxonomy = null;
        }

        return $localTaxonomy;
    }

    public static function getPagerPage($request)
    {
        if ($request->query('pager_page')) {
            $page = $request->query('pager_page');;
        } else {
            $page = '1';
        }

        return $page;
    }
}
