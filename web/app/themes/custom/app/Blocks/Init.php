<?php

namespace App\Blocks;


use Timber\Image;
use Timber\Timber;

class Init
{
    /**
     * @var string
     */
    private $blocks = [];

    function __construct()
    {
        $this->getBlocks();
        $this->getBlockVariations();
    }

    private function getBlocks(): void
    {
        collect(glob(get_template_directory() . '/dist/blocks/*/index.php'))
                ->map(function ($file) {
                    require_once($file);
                });
    }

    private function getBlockVariations(): void
    {
        collect(glob(get_template_directory() . '/src/block-variations/*/index.php'))
            ->map(function ($file) {
                require_once($file);
            });
    }
}
