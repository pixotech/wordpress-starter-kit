<?php

use App\ACF\FieldsBuilder;



        $builder = new FieldsBuilder('block_link_cards');

        $builder
            ->addText('header')
            ->setWidth("60%")
            ->setRequired();

        $builder->addTrueFalse('hide_header', [
            'ui' => 1,
        ])
            ->setLabel('Hide header')
            ->setWidth("40%")
            ->setInstructions('The header is required for screen readers but can be hidden from view if you like.');

        $builder->addInlineRichText('description');
        $builder->setLocation('block', '==', 'acf/link-cards');

        return $builder;
