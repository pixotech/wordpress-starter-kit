<?php


use App\ACF\FieldsBuilder;

$builder = new FieldsBuilder('block_link_card');


$imageField = new FieldsBuilder('image_field');
$imageField->addConditionalImage('image');

$builder
    ->addText('link_text', [
        'label' => 'Link Text',
    ])
    ->addPageLink('link_reference', ['label' => 'Link Reference'])
    ->addText('description', [
        'label' => 'Description',
    ])
    ->addFields($imageField);

$builder->setLocation('block', '==', 'acf/link-card');

return $builder;
