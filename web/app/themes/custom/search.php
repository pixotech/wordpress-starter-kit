<?php

/**
 * Search results page
 */

namespace App;

use App\Http\Controllers\Controller;
use App\ViewModels\PageViewModel;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class SearchController extends Controller
{
    public function handle()
    {
        $context = $this->getContext();
        $context['post']['title'] = 'Search';
        $searchQuery = get_search_query();

        $args  = array(
            's' => $searchQuery,
            'posts_per_page' => 20,
            'orderby' => 'relevance',
            'post_types'  => [
                'page',
                'news',
                'event',
            ],
            'relevanssi'  => true,
            'relevanssi_highlight' => true,
            'relevanssi_excerpts' => true,
//            'paged' => $paged,
        );
        $searchResults = Timber::get_posts($args);
        $context['search']['results'] = $searchResults;
        $context['search']['count'] = count($searchResults);
        $context['search']['term'] = $searchQuery;

        return new TimberResponse('patterns/pages/search/search.twig', $context);
    }
}
