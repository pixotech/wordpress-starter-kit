document.addEventListener("DOMContentLoaded", function() {
    const taxonomyFilterForm = document.querySelector('[data-taxonomy-filter]')

    if (taxonomyFilterForm) {
        const resetButton = taxonomyFilterForm.querySelector('button[type="reset"]')

        if (doesQueryParameterExist('global_taxonomy')) {
            resetButton.addEventListener('click', () => {
                window.location = window.location.pathname
            })
        }

        function doesQueryParameterExist(field) {
            const url = window.location.href;
            if (url.indexOf('?' + field + '=') !== -1) return true
            return url.indexOf('&' + field + '=') !== -1;
        }
    }
});
