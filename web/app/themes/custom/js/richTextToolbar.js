wp.domReady(() => {

    wp.richText.unregisterFormatType('core/code')
    wp.richText.unregisterFormatType('core/keyboard')
    wp.richText.unregisterFormatType('core/image')
    wp.richText.unregisterFormatType('core/language')
    wp.richText.unregisterFormatType('core/footnote')
    wp.richText.unregisterFormatType('core/text-color')

    const addDataAttributeWrapper = (BlockEdit) => {
        return (props) => {
            let Wrapper = null;
            if (['core/paragraph'].includes(props.name)) {
                Wrapper = wp.element.createElement(
                    'div',
                    {'data-cy-block': 'text'},
                    null
                );
            } else if (['core/heading'].includes(props.name)) {
                Wrapper = wp.element.createElement(
                    'div',
                    {'data-cy-block': 'heading'},
                    null
                );
            } else {
                // Return the original BlockEdit if the block type doesn't match
                return wp.element.createElement(BlockEdit, props);
            }

            return wp.element.cloneElement(
                Wrapper,
                {},
                wp.element.createElement(BlockEdit, props)
            );
        };
    };

    wp.hooks.addFilter(
        'editor.BlockEdit',
        'custom/add-data-attribute-wrapper',
        addDataAttributeWrapper
    );
});
