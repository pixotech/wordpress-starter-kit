document.addEventListener('DOMContentLoaded', function() {
    // Select all color squares
    const colorSquares = document.querySelectorAll('.color-square');

    // Loop over each color square
    colorSquares.forEach(colorSquare => {
        // Get the computed background color
        const backgroundColor = getComputedStyle(colorSquare).backgroundColor;

        // Convert RGB color to Hex color
        const hexColor = rgbToHex(backgroundColor);

        // Find the corresponding .color-data__hexCode element
        const hexCodeElement = colorSquare.parentElement.querySelector('.color-data__hexCode');

        // Set the text content of the .color-data__hexCode element to the background color
        hexCodeElement.textContent = hexColor;
    });

    // Function to convert RGB color to Hex color
    function rgbToHex(rgb) {
        const match = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return "#" + ((1 << 24) + (+match[1] << 16) + (+match[2] << 8) + +match[3]).toString(16).slice(1).toUpperCase();
    }
});
