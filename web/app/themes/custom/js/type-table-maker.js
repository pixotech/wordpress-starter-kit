const styleTables = Array.from(document.querySelectorAll('.style-table'));
styleTables.forEach(setUpStyleTable);

function setUpStyleTable(container) {
    const typeStyles = Array.from(container.querySelectorAll('.type-style'));

    const sorted = typeStyles.sort((a, b) => {
        const fontSizeA = parseInt(getComputedStyle(a).fontSize, 10);
        const fontSizeB = parseInt(getComputedStyle(b).fontSize, 10);
        return fontSizeB - fontSizeA;
    });

    typeStyles.forEach(e => e.remove());

    const table = document.createElement('table');
    container.append(table);
    container.append(document.createElement('hr'));

    sorted.forEach(e => {
        const tr = document.createElement('tr');
        table.append(tr);

        makeTd(e.dataset.name, '200px');

        const td = document.createElement('td');
        tr.append(td);
        td.append(e);

        const computedStyle = getComputedStyle(e);
        const fontSize = computedStyle.fontSize;
        const lineHeight = computedStyle.lineHeight;
        const fontFamily = computedStyle.fontFamily;
        const fontWeight = computedStyle.fontWeight;

        makeTd(`${fontSize}/${lineHeight}`, '120px', 'left', 'monospace');
        makeTd(fontWeight, '50px', 'center');
        makeTd(fontFamily, '200px');

        function makeTd(content, width = '100px', textAlign = 'left', fontFamily = '') {
            const td = document.createElement('td');
            tr.append(td);
            td.textContent = content;
            td.style.width = width;
            td.style.textAlign = textAlign;
            td.style.fontFamily = fontFamily;
        }
    });
}
