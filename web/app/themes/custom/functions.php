<?php

use App\ACF\ACFBuilderUtilities;
use App\ACF\OptionPages;
use App\ACF\WysiwygEditors;
use App\Blocks\Init as BlockInit;
use App\Http\Lumberjack;
use App\Utilities\ManageMimeTypes;
use App\Utilities\RegisterTaxonomies;


require_once __DIR__ . '/vendor/autoload.php';

// Create the Application Container
$app = require_once('bootstrap/app.php');

// Bootstrap Lumberjack from the Container
$lumberjack = $app->make(Lumberjack::class);
$lumberjack->bootstrap();

// Import our routes file
require_once('routes.php');

// Enqueue scripts and styles
require_once('app/Utilities/EnqueueAssets.php');

// Initialize image dimensions
require_once('app/Utilities/InitializeImageDimensions.php');

// Set global params in the Timber context
add_filter('timber_context', [$lumberjack, 'addToContext']);

OptionPages::initialize();

ACFBuilderUtilities::initialize();

WysiwygEditors::initialize();

new BlockInit;

ManageMimeTypes::addMimeType('svg', 'image/svg+xml');

/* Disable XMLrpc.php for added security */
add_filter( 'xmlrpc_enabled', '__return_false' );

global $wp_rewrite;
$wp_rewrite->set_permalink_structure('/%postname%/');
update_option("rewrite_rules", false);

# Add registerComponentViewModels
/* registerComponentViewModels(); */
/* function registerComponentViewModels() */
/* { */
/*     $files = collect(glob(get_template_directory() . '/app/ViewModels/Components/*.php'))->toArray(); */
/*     foreach ($files as $file) { */
/*         // get_declared_classes() will only show classes that have been required/loaded. */
/*         require_once($file); */
/*     } */
/* } */

add_action(
    'init',
    function () {
        // Check if the menu exists
        $menu_name = 'Main Navigation';
        $menu_exists = wp_get_nav_menu_object($menu_name);

        // If it doesn't exist, let's create it.
        if (!$menu_exists) {
            $menu_id = wp_create_nav_menu($menu_name);
        }
    }
);

add_action(
    'init',
    function () {
        // Check if the menu exists
        $menu_name = 'Footer Links';
        $menu_exists = wp_get_nav_menu_object($menu_name);

        // If it doesn't exist, let's create it.
        if (!$menu_exists) {
            $menu_id = wp_create_nav_menu($menu_name);
        }
    }
);

add_action(
    'init',
    function () {
        // Check if the menu exists
        $menu_name = 'Eyebrow Menu';
        $menu_exists = wp_get_nav_menu_object($menu_name);

        // If it doesn't exist, let's create it.
        if (!$menu_exists) {
            $menu_id = wp_create_nav_menu($menu_name);
        }
    }
);





//function news_block_render_callback( $block, $content = '', $is_preview = false )
//{
//
//    $context = Timber::context();
//
//// Store block values.
//    $context['block'] = $block;
//
//// Store field values.
//    $context['fields'] = get_fields();
//    $newsQuery = News::builder()
//        ->orderBy('date', 'desc')
//        ->limit(3)
//        ->get();
//
//    $post = new Timber\Post();
//    $context['posts'] =  collect($newsQuery)->map(function ($item) {
//        return NewsStoryViewModel::createFromPost($item);
//    });
//
//
//// Store $is_preview value.
//    $context['is_preview'] = $is_preview;
//
//// Render the block.
//    Timber::render('patterns/blocks/news-mini/news-mini.twig', $context);
//}

//block editor tweaks
add_filter(
    'block_editor_settings',
    function ($editor_settings) {
        $editor_settings['__experimentalFeatures']['typography']['dropCap'] = false;
        return $editor_settings;
    }
);

// my-plugin.php
//
//function filter_block_categories_when_post_provided($block_categories, $editor_context)
//{
//    if (!empty($editor_context->post)) {
//        array_push(
//            $block_categories,
//            array(
//                'slug' => 'components',
//                'title' => __('Components', 'components'),
//                'icon' => null,
//            )
//        );
//    }
//    return $block_categories;
//}
//
//add_filter('block_categories_all', 'filter_block_categories_when_post_provided', 10, 2);
RegisterTaxonomies::initialize('units_and_areas', array('news', 'profile'), true );
RegisterTaxonomies::initialize('news_types', array('news'));
RegisterTaxonomies::initialize('profile_types', array('profile'));

function page_register_post_meta() {
    register_post_meta( 'page', 'page_layout', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
}
add_action( 'init', 'page_register_post_meta' );


function news_register_post_meta() {
    register_post_meta( 'news', 'news_meta_block_summary', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'news', 'news_meta_block_image_id', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'integer',
    ) );
    register_post_meta( 'news', 'news_meta_block_focal_point', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'news', 'news_meta_block_authors', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
}
add_action( 'init', 'news_register_post_meta' );

function profile_register_post_meta() {
    register_post_meta( 'profile', 'profile_meta_block_prefix', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_first_name', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_last_name', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_titles', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_image_id', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'integer',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_focal_point', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_email', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
    register_post_meta( 'profile', 'profile_meta_block_phone_number', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
}
add_action( 'init', 'profile_register_post_meta' );

add_filter( 'allowed_block_types_all', 'modify_allowed_block_types', 25, 2 );

function modify_allowed_block_types( $allowed_blocks, $editor_context ) {
    if( 'news' === $editor_context->post->post_type ) {
        $allowed_blocks = array(
            'core/image',
            'core/paragraph',
            'core/heading',
            'core/list'
        );
        return $allowed_blocks;
    }
    elseif( 'profile' === $editor_context->post->post_type ) {
        $allowed_blocks = array(
            'custom/link-list',
            'custom/list-link',
            'core/image',
            'core/paragraph',
            'core/heading',
            'core/list',
        );
        return $allowed_blocks;
    }
    else {
        return;
    }
}
