import {Given, Then, When} from "@badeball/cypress-cucumber-preprocessor";
import BlocksDsl from "../../dsl/BlocksDsl";

let blocksDsl;

Given('that I am viewing the kitchen sink page', () => {
    blocksDsl = new BlocksDsl;
});

Then('I should see the title', () => {
    blocksDsl.verifyTitleExistOnPage('Title');
});

When('I see the {string}', (dataCyBlock) => {
    blocksDsl.verifyBlockExistOnPage(dataCyBlock);
});

When('{string} equals {string}', (dataCyToggle, value) => {
    blocksDsl.verifyToggleTypeExists(dataCyToggle, value);
});

Then('I should see the {string}', (dataCyAttribute) => {
    blocksDsl.verifyPropertyExistOnPage(dataCyAttribute);
});
