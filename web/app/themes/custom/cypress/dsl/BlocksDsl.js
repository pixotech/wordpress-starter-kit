import DSL from './dsl';

export default class BlocksDsl extends DSL {
    visit() {
        cy.visit('http://starter-kit-test.localhost/sample-page');
    }
    verifyTitleExistOnPage(value) {
        cy.contains(value).should('exist');
    }
    verifyBlockExistOnPage(value) {
        cy.get(`[data-cy='${value}']`).should('exist');
    }
    verifyToggleTypeExists(name, value) {
        if(name && value) {
            // cy.get(`[data-cy='${block}'][data-${name}="${value}"]`).should('exist');
            cy.get(`[data-${name}="${value}"]`).should('exist');
        }
    }
    verifyPropertyExistOnPage(value) {
        cy.get(`[data-cy='${value}']`).should('exist');
    }
}
