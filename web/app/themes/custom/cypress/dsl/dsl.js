export default class DSL {
    constructor(visitPage = true) {
        this.setViewport();

        if(visitPage) {
            this.visit();
        }
    }

    visit() {}

    setViewport() {
        cy.viewport(1920, 1080);
    }
}
