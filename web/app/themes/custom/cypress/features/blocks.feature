Feature: Custom Content Blocks Kitchen Sink Page

    Scenario Outline: View kitchen sink page
        Given that I am viewing the kitchen sink page
        When I see the "<data-cy-block>"
        When "<data-cy-toggle>" equals "<value>"
        Then I should see the "<data-cy-attribute>"

    Examples:
        | data-cy-block     | data-cy-attribute             | data-cy-toggle    | value     |
        | link-cards        | link-cards__heading           |                   |           |
        | link-cards        | link-cards__description       |                   |           |
        | link-cards        | link-cards__link              |                   |           |
        | link-cards        | link-cards__card-description  |                   |           |
        | link-cards        | link-cards__image             | show-image        | true      |
        | call-to-action    | call-to-action__lead-in-text  |                   |           |
        | call-to-action    | call-to-action__button        | is-jumbo          | false     |
        | call-to-action    | call-to-action__link-list     | is-jumbo          | true      |
        | link-lists        | link-list__list-name          |                   |           |
        | link-lists        | link-list__links              |                   |           |
        | giving-form       | fund__name                    |                   |           |
        | giving-form       | fund__description             |                   |           |
        | giving-form       | fund__submit-button           |                   |           |
        | giving-form       | giving-form__header           | show-heading      | true      |
        | quote             | quote__image                  |                   |           |
        | quote             | quote__quote                  |                   |           |
        | quote             | quote__attribution            |                   |           |
        | quote             | quote__button                 | show-cta          | true      |
        | youtube-embed     |                               |                   |           |
        | vimeo-embed       |                               |                   |           |
        | text              |                               |                   |           |
        | heading           |                               |                   |           |
        | image-block       |                               |                   |           |
