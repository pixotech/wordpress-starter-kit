<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 */

namespace App;

use App\Http\Controllers\Controller;
use App\ViewModels\NewsStoryViewModel;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class SingleNewsController extends Controller
{
    public function handle()
    {
        $context = $this->getContext();
        $post = new Post();
        $context['post'] = NewsStoryViewModel::createFromPost($post);

        return new TimberResponse('patterns/pages/news/news-post/news-post.twig', $context);
    }
}
