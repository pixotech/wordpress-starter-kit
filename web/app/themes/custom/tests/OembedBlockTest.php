<?php declare(strict_types=1);

namespace tests;

use PHPUnit\Framework\TestCase;

//require_once __DIR__ . "/../src/blocks/oembed/functions.php";
use Custom\Blocks\Oembed\OembedBlock;

final class OembedBlockTest extends TestCase
{
    public function testBlankUrl(): void
    {
        $oembedBlock = new OembedBlock();
        $url = "";
        $result = $oembedBlock->extractVideoIdFromYoutubeThumbnailUrl($url);
        $this->assertSame("__missingThumbnailUrl__", $result);
    }

    public function testDefaultThumbnailUrl(): void
    {
        $url = "https://i.ytimg.com/vi/3cynqSLcV7o/hqdefault.jpg";
        $result = extractVideoIdFromYoutubeThumbnailUrl($url);
        $this->assertSame("3cynqSLcV7o", $result);
    }

    public function testMaxQualityThumbnailUrl(): void
    {
        $url = "https://i.ytimg.com/vi/3cynqSLcV7o/mqdefault.jpg";
        $result = extractVideoIdFromYoutubeThumbnailUrl($url);
        $this->assertSame("3cynqSLcV7o", $result);
    }
}
