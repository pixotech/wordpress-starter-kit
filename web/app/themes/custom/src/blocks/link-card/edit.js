import {__} from '@wordpress/i18n';
import {
    RichText,
    BlockControls,
    useBlockProps,
    InspectorControls
} from '@wordpress/block-editor';
import {PanelBody, ToggleControl} from '@wordpress/components';
import {Link, Image, MediaToolbar} from '@10up/block-components';


const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {linkText, linkUrl, opensInNewTab, description, imageId, focalPoint, showImage} = attributes;

    const blockProps = useBlockProps(
        {
            className: 'link-cards__card',
        }
    );

    const handleTextChange = value => setAttributes({linkText: value});
    const handleDescriptionChange = value => setAttributes({description: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });
    const handleImageSelect = (image) => {
        setAttributes({imageId: image.id});
    }

    const handleImageRemove = () => {
        setAttributes({imageId: null})
    }

    const handleFocalPointChange = (value) => {
        console.log(value);
        setAttributes({focalPoint: value});
    }

    return (
        <>
            <li {...blockProps}>
                {showImage && (
                    <div className="link-cards__image-wrapper">
                        <BlockControls>
                            <MediaToolbar
                                isOptional
                                id={imageId}
                                onSelect={handleImageSelect}
                                onRemove={handleImageRemove}
                            />
                        </BlockControls>
                        <Image
                            id={imageId}
                            className="link-cards__image"
                            size="full"
                            onSelect={handleImageSelect}
                            focalPoint={focalPoint}
                            onChangeFocalPoint={handleFocalPointChange}
                            labels={{
                                title: 'Select Poster Image',
                                instructions: 'Upload a media file or pick one from your media library.'
                            }}
                        />
                    </div>
                )}
                <div className="link-cards__card-text">
                    <div className="link-cards__card-cta">
                        <Link
                            value={linkText}
                            url={linkUrl}
                            opensInNewTab={opensInNewTab}
                            onTextChange={handleTextChange}
                            onLinkChange={handleLinkChange}
                            onLinkRemove={handleLinkRemove}
                            className='link-cards__card-link'
                            placeholder='Enter Link Text here...'
                        />
                    </div>
                    <RichText
                        tagName="p"
                        placeholder={__(
                            'Write card description text…',
                            'gutenberg-examples'
                        )}
                        value={description}
                        onChange={handleDescriptionChange}
                        className="link-cards__card-description"
                        allowedFormats={['core/bold', 'core/italic']}
                    />

                </div>
            </li>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show image', 'custom')}
                        checked={showImage}
                        onChange={() => setAttributes({showImage: !showImage})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};


export default Edit;
