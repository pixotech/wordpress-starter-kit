import {__} from '@wordpress/i18n';
import {
    RichText,
    useInnerBlocksProps,
    InspectorControls,
    useBlockProps
} from '@wordpress/block-editor';
import {PanelBody, ToggleControl} from '@wordpress/components';
import {Link} from '@10up/block-components';

const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {linkText, linkUrl, opensInNewTab, leadInText, isJumbo, showDescription, description} = attributes;

    const blockProps = useBlockProps(
        {
            className: 'call-to-action',
        }
    );

    const innerBlocksProps = useInnerBlocksProps(
        {
            className: 'call-to-action__link-list',
        },
        {
            allowedBlocks: [
                'custom/link-with-icon',
            ],
            template: [
                ['custom/link-with-icon'],
            ],
        }
    );

    const handleTextChange = value => setAttributes({linkText: value});
    const handleLeadInTextChange = value => setAttributes({leadInText: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });
    const handleDescriptionChange = value => setAttributes({description: value});

    return (
        <>
            <div {...blockProps}>

                <div>
                    <RichText
                        tagName="p"
                        placeholder={__(
                            'A brief (3-5 words) message that sets up the call to action button, enticing users to click through.',
                            'custom'
                        )}
                        value={leadInText}
                        onChange={handleLeadInTextChange}
                        className="call-to-action__lead-in-text"
                        allowedFormats={['core/bold', 'core/italic']}
                    />
                    {!isJumbo && (
                        <div className="call-to-action__button">
                            <Link
                                value={linkText}
                                url={linkUrl}
                                opensInNewTab={opensInNewTab}
                                onTextChange={handleTextChange}
                                onLinkChange={handleLinkChange}
                                onLinkRemove={handleLinkRemove}
                                className='standard-button'
                                placeholder='A very short (2-3 words) verb-driven phrase. Use a clear and specific verb like "Apply," "Explore," or "Connect."'
                            />
                        </div>
                    )}
                    { isJumbo && showDescription && (
                        <RichText
                            tagName="p"
                            placeholder={__(
                                'A brief description'
                            )}
                            value={description}
                            onChange={handleDescriptionChange}
                            className="call-to-action__description"
                            allowedFormats={['core/bold', 'core/italic']}
                        />
                    )}
                    {isJumbo && (
                        <ul {...innerBlocksProps} />
                    )}
                </div>
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Is Jumbo', 'custom')}
                        checked={isJumbo}
                        onChange={() => setAttributes({ isJumbo: !isJumbo })}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                    <ToggleControl
                        label={__('Show description', 'custom')}
                        checked={showDescription}
                        onChange={() => setAttributes({ showDescription: !showDescription })}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};


export default Edit;
