<?php
/**
 * Plugin Name: Gutenberg Examples Dynamic Block
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin demonstrating how to register new blocks for the Gutenberg editor.
 * Version: 1.1.0
 * Author: the Gutenberg Team
 *
 * @package gutenberg-examples
 */

use Timber\Timber;
use Timber\Image;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 */
function call_to_action_block_init() {

    register_block_type(
        __DIR__,
        array(
            'render_callback' => 'call_to_action_block_render_callback',
        )
    );
}
add_action( 'init', 'call_to_action_block_init' );


/**
 * This function is called when the block is being rendered on the front end of the site
 *
 * @param array    $attributes     The array of attributes for this block.
 * @param string   $content        Rendered block output. ie. <InnerBlocks.Content />.
 * @param WP_Block $block_instance The instance of the WP_Block class that represents the block being rendered.
 */
function call_to_action_block_render_callback( $attributes, $content, $block_instance ) {

    ob_start();

    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;

    // Store field values.

    $context['fields']['leadInText'] = $attributes['leadInText'];
    $context['fields']['linkText'] = $attributes['linkText'];
    $context['fields']['linkReference'] = $attributes['linkUrl'];
    $context['fields']['isJumbo'] = $attributes['isJumbo'];
    $context['fields']['showDescription'] = $attributes['showDescription'];
    if($context['fields']['showDescription']) {
        $context['fields']['description'] = $attributes['description'];
    }
    $context['content'] = $content;


    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/call-to-action/call-to-action.twig', $context);

    return ob_get_clean();
}
