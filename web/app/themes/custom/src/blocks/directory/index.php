<?php
/**
 * Plugin Name: Gutenberg Examples Dynamic Block
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin demonstrating how to register new blocks for the Gutenberg editor.
 * Version: 1.1.0
 * Author: the Gutenberg Team
 *
 * @package gutenberg-examples
 */

use App\Http\Controllers\Controller as BaseController;
use App\Utilities\UrlHelper;
use App\ViewModels\Profile\DirectoryViewModel;
use App\ViewModels\Profile\ProfileCardViewModel;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 */
function directory_block_init() {

    register_block_type(
        __DIR__,
        array(
            'render_callback' => 'directory_block_render_callback',
        )
    );
}
add_action( 'init', 'directory_block_init' );


/**
 * This function is called when the block is being rendered on the front end of the site
 *
 * @param array    $attributes     The array of attributes for this block.
 * @param string   $content        Rendered block output. ie. <InnerBlocks.Content />.
 * @param WP_Block $block_instance The instance of the WP_Block class that represents the block being rendered.
 */
function directory_block_render_callback( $attributes, $content, $block_instance ) {
    ob_start();
    $postId = get_the_ID();
    $post = new Post($postId);

    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;
    $context['fields'] = $attributes;
    $context['fields']['filterByGlobalTaxonomy'] = $attributes['filterByGlobalTaxonomy'];
    if($context['fields']['filterByGlobalTaxonomy']) {
        $context['fields']['globalTaxonomy'] = $attributes['globalTaxonomy'];
    }
    $context['fields']['globalOptions']  = formatProfileTaxonomyOptions($attributes['globalOptions']);
    $context['fields']['localOptions']  = formatProfileTaxonomyOptions($attributes['localOptions']);
    // Store field values.
    $context['content'] = $content;
    $context['post'] = DirectoryViewModel::createFromPost($post);

    $viewModelBuilder = function($news) {
        return ProfileCardViewModel::createFromPost($news);
    };

    $request = $context['request'];

    $taxonomies = getProfileTaxonomiesFromQuery($context['post'], $request);
    $context['fields']['taxonomies'] = $taxonomies;

    $taxQueryOptions = [];

   if($taxonomies['globalTaxonomy'] || ($context['fields']['filterByGlobalTaxonomy'] && $context['fields']['globalTaxonomy']) ) {
        $taxQueryOptions[] = [
            'taxonomy' => 'units_and_areas',
            'field' => 'term_id',
            'terms' => [$taxonomies['globalTaxonomy'], $context['fields']['globalTaxonomy']]
        ];
    }

    if($taxonomies['localTaxonomy']) {
        $taxQueryOptions[] = [
            'taxonomy' => 'profile_types',
            'field' => 'term_id',
            'terms' => $taxonomies['localTaxonomy']
        ];
    }

    $queryOptions = [
        'orderby' => 'post_date',
        'order' => 'DESC',
        'tax_query' => $taxQueryOptions,
    ];

    BaseController::addPagerToContext($context, 'profile', $viewModelBuilder, $queryOptions);

    if(count($context['post']->list) === 0) {
        $context['listingError'] = [
            'heading' => 'No profiles found',
            'message' => 'Try browsing all people, or searching the site for a specific person.'
        ];
    }

    Timber::render(get_template_directory() .'/patterns/blocks/directory/directory.twig', $context);

    return ob_get_clean();
}

function formatProfileTaxonomyOptions($taxonomyOptions) {
    $options = [];

    foreach($taxonomyOptions as $option) {
        $options[] = [
            'label' => $option["name"],
            'value' => $option["id"]
        ];
    }
    return $options;
}

function getProfileTaxonomiesFromQuery($list, $request) {
    $filters = UrlHelper::getTaxonomyFilters($request);

    return $filters;
}
