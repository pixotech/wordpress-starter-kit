import {__} from '@wordpress/i18n';
import {
    BlockControls,
    RichText,
    useBlockProps,
} from '@wordpress/block-editor';
import {Image, MediaToolbar} from '@10up/block-components';
import {useEntityProp} from '@wordpress/core-data';

const Edit = (props) => {
    const [meta, setMeta] = useEntityProp('postType', 'news', 'meta');

    const metaFieldSummary = meta['news_meta_block_summary'];
    const metaFieldFocalPoint = meta['news_meta_block_focal_point'];
    const metaFieldFeaturedImage = meta['news_meta_block_image_id'];

    const updateMetaSummaryValue = (newValue) => {
        setMeta({...meta, news_meta_block_summary: newValue});
    };

    const updateMetaFocalPointValue = (newValue) => {
        setMeta({...meta, news_meta_block_focal_point: JSON.stringify(newValue) });
        console.log(JSON.stringify(newValue));
    };

    const updateMetaFeaturedImageValue = (newValue) => {
        setMeta({...meta, news_meta_block_image_id: newValue.id});
    };

    const removeMetaFeaturedImageValue = () => {
        setMeta({...meta, news_meta_block_image_id: null});
    };

    const blockProps = useBlockProps( {
        className: 'news-post',
    } );

    return (
        <div {...blockProps}>
            <BlockControls>
                <MediaToolbar
                    isOptional
                    id={metaFieldFeaturedImage}
                    onSelect={ updateMetaFeaturedImageValue }
                    onRemove={ removeMetaFeaturedImageValue }
                />
            </BlockControls>
            <Image
                id={metaFieldFeaturedImage}
                className="my-image"
                size="full"
                onSelect={updateMetaFeaturedImageValue}
                focalPoint={ metaFieldFocalPoint ? JSON.parse(metaFieldFocalPoint) : ''}
                onChangeFocalPoint={updateMetaFocalPointValue}
                labels={{
                    title: 'Select Featured Image',
                    instructions: 'Add an eye-catching image to add visual interest to the story. Choose a clear, compelling picture that represents the topic. Optimize the file size for faster loading.'
                }}
            />
            <RichText
                tagName="p"
                className="news__summary"
                placeholder={__('A summary (about 25 words) of the story. Limit 200 characters.', 'custom')}
                value={metaFieldSummary}
                onChange={updateMetaSummaryValue}
            />
        </div>
    );
};

export default Edit;
