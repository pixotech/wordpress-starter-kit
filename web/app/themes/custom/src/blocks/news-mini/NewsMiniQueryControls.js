import {QueryControls} from '@wordpress/components';
import {withState} from '@wordpress/compose';

let customQuery = ({orderBy, order, category, categories, numberOfItems, setState, onTaxonomyChange, onNumberChange}) => {
    return (
        <QueryControls
            {...{order, numberOfItems}}
            categoriesList={categories}
            selectedCategoryId={category}
            onCategoryChange={(category) => {
                setState({category})
                onTaxonomyChange(category);
            }}
            numberOfItems={numberOfItems}
            onNumberOfItemsChange={(numberOfItems) => {
                setState({numberOfItems})
                onNumberChange(numberOfItems);
            }}
        />
    );
};
const NewsMiniQueryControls = withState({
    orderBy: 'title',
    order: 'asc',

})(customQuery);


export default NewsMiniQueryControls;
