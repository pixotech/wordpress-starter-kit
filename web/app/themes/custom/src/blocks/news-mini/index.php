<?php
/**
 * Plugin Name: Gutenberg Examples Dynamic Block
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin demonstrating how to register new blocks for the Gutenberg editor.
 * Version: 1.1.0
 * Author: the Gutenberg Team
 *
 * @package gutenberg-examples
 */

use App\ViewModels\NewsCardViewModel;
use App\ViewModels\NewsListingViewModel;
use Rareloop\Lumberjack\Post;
use Timber\PostQuery;
use Timber\Timber;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 */
function news_mini_block_init() {

    register_block_type(
        __DIR__,
        array(
            'render_callback' => 'news_mini_block_render_callback',
        )
    );
}
add_action( 'init', 'news_mini_block_init' );


/**
 * This function is called when the block is being rendered on the front end of the site
 *
 * @param array    $attributes     The array of attributes for this block.
 * @param string   $content        Rendered block output. ie. <InnerBlocks.Content />.
 * @param WP_Block $block_instance The instance of the WP_Block class that represents the block being rendered.
 */
function news_mini_block_render_callback( $attributes, $content, $block_instance ) {
    ob_start();
    $postId = get_the_ID();
    $post = new Post($postId);

    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;
    $context['fields'] = $attributes;
    $context['fields']['showTitle'] = $attributes['showTitle'];
    $context['fields']['title'] = $attributes['title'];
    $context['fields']['number'] = $attributes['number'];
    $context['fields']['globalTaxonomy'] = $attributes['globalTaxonomy'];
    $context['fields']['showCTALink'] = $attributes['showCTALink'];
    if($context['fields']['showCTALink']) {
        $context['fields']['linkText'] = $attributes['linkText'];
        $context['fields']['linkUrl'] = $attributes['linkUrl'];
    }

    // Store field values.
    $context['content'] = $content;
    $context['post'] = NewsListingViewModel::createFromPost($post);

    $viewModelBuilder = function($news) {
        return NewsCardViewModel::createFromPost($news);
    };

    $taxQueryOptions = [];

    if($context['fields']['globalTaxonomy']) {
        $taxQueryOptions[] = [
            'taxonomy' => 'units_and_areas',
            'field' => 'term_id',
            'terms' => $context['fields']['globalTaxonomy']
        ];
    }

    $queryOptions = [
        'orderby' => 'post_date',
        'order' => 'DESC',
        'tax_query' => $taxQueryOptions,
    ];

    $baseQueryOptions = [
        'post_type' => 'news',
        'posts_per_page' => $context['fields']['number'],
        'paged' => 1
    ];

    $query = new PostQuery(array_merge($baseQueryOptions, $queryOptions));
    $context['fields']['items'] = collect($query->get_posts())
        ->map($viewModelBuilder)
        ->toArray();

    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/news-mini/news-mini.twig', $context);

    return ob_get_clean();
}
