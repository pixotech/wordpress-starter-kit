import {__} from '@wordpress/i18n';
import {RichText, InspectorControls, useBlockProps} from '@wordpress/block-editor';
import {Spinner, PanelBody, ToggleControl, __experimentalNumberControl as NumberControl} from '@wordpress/components';
import {Link, useAllTerms} from "@10up/block-components";
import NewsMiniQueryControls from "./NewsMiniQueryControls";

const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {title, linkText, linkUrl, showTitle, showCTALink, globalTaxonomy, number} = attributes;

    const blockProps = useBlockProps({
        className: 'news-mini',
    });

    const handleNumberChange = value => {
        setAttributes({number: value});
    }
    const handleGlobalTaxonomyChange = value => {
        setAttributes({globalTaxonomy: value});
    }
    const [globalOptions, hasResolvedGlobalOptions] = useAllTerms('units_and_areas');
    const [localOptions, hasResolvedLocalOptions] = useAllTerms('news_types');

    if (hasResolvedGlobalOptions && globalOptions.length > 0) {
        setAttributes({globalOptions: globalOptions});
    }

    if (hasResolvedLocalOptions && localOptions.length > 0) {
        setAttributes({localOptions: localOptions});
    }
    const handleTextChange = value => setAttributes({linkText: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });


    return (
        <>
            <div {...blockProps}>
                {showTitle && (
                    <RichText
                        tagName="h2"
                        placeholder={__(
                            'The title is required for screen readers, but can be hidden from view if you like.',
                            'custom'
                        )}
                        value={title}
                        onChange={(title) => setAttributes({title})}
                        className="news-mini__title"
                        allowedFormats={[]}
                    />
                )}
                {!hasResolvedGlobalOptions && <Spinner/>}
                {hasResolvedGlobalOptions && globalOptions.length === 0 && <p>No global taxonomy found.</p>}
                {hasResolvedGlobalOptions && globalOptions.length > 0 && (
                    <NewsMiniQueryControls
                        categories={globalOptions}
                        category={globalTaxonomy}
                        onTaxonomyChange={handleGlobalTaxonomyChange}
                        onNumberChange={handleNumberChange}
                        numberOfItems={number}
                    />
                )}
                {showCTALink && (
                    <div className="news-mini__cta-container">
                        <Link
                            value={linkText}
                            url={linkUrl}
                            onTextChange={handleTextChange}
                            onLinkChange={handleLinkChange}
                            onLinkRemove={handleLinkRemove}
                            className='standard-button'
                            placeholder='A very short (2-3 words) verb-driven phrase. Use a clear and specific verb like "Apply," "Explore," or "Connect."'
                        />
                    </div>
                )}
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show title', 'custom')}
                        checked={showTitle}
                        onChange={() => setAttributes({showTitle: !showTitle})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'gutenberg-lessons',
                        )}
                    />
                    <ToggleControl
                        label={__('Show CTA button', 'custom')}
                        checked={showCTALink}
                        onChange={() => setAttributes({showCTALink: !showCTALink})}
                        help={__(
                            'Enable call to action button on block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};

export default Edit;
