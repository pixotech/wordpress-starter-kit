import {__} from '@wordpress/i18n';
import {RichText, useBlockProps, useInnerBlocksProps} from '@wordpress/block-editor';

export default function Edit(props) {
    const {attributes, setAttributes} = props;

    const {heading} = attributes;

    const blockProps = useBlockProps({
        className: 'link-list',
    });

    const innerBlocksProps = useInnerBlocksProps(
        {
            className: 'link-list__links',
        },
        {
            allowedBlocks: [
                'custom/list-link',
            ],
        },
        {
            template: [
                ['custom/list-link'],
            ],
        });
    return (
        <ul {...blockProps}>
            <RichText
                tagName="h2"
                placeholder={__(
                    'List heading…',
                    'custom'
                )}
                value={heading}
                onChange={(heading) => setAttributes({heading})}
                className="link-list__heading"
                allowedFormats={['core/bold', 'core/italic']}
            />
            <ul {...innerBlocksProps} />
        </ul>
    )
        ;
}
