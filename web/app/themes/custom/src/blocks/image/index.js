import { registerBlockType } from '@wordpress/blocks';
import { BlockContent } from '@wordpress/block-editor'

import Edit  from './edit';
import metadata from './block.json';

registerBlockType( metadata.name, {
    edit: Edit,
    save: () => <BlockContent />
} );
