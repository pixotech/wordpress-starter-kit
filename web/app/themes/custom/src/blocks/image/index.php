<?php

use Timber\Image;
use Timber\Timber;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 */

function image_block_init() {
    register_block_type(
        __DIR__,
        array(
            'render_callback' => 'image_block_render_callback',
        )
    );
}
add_action( 'init', 'image_block_init' );


/**
 * This function is called when the block is being rendered on the front end of the site
 *
 * @param array    $attributes     The array of attributes for this block.
 * @param string   $content        Rendered block output. ie. <InnerBlocks.Content />.
 * @param WP_Block $block_instance The instance of the WP_Block class that represents the block being rendered.
 */
function image_block_render_callback( $attributes, $content, $block_instance ) {

    ob_start();

    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;

    $image = new Image($attributes['imageId']);

    // Store field values.
    $context['image'] = $attributes;
    $context['image']['src'] = $image->src();
    $context['image']['alt'] = $image->alt();

   // dd($context['fields']);
    $context['content'] = $content;

    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/image/image.twig', $context);

    return ob_get_clean();
}
