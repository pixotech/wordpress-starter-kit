import { __ } from '@wordpress/i18n';
import { BlockControls, useBlockProps, InspectorControls } from '@wordpress/block-editor';
import { PanelBody, TextControl, SelectControl } from '@wordpress/components';
import { Image, MediaToolbar } from '@10up/block-components';

export default function Edit(props) {
    const {
        attributes,
        setAttributes
    } = props;

    const { imageId, focalPoint, altText, caption, align } = attributes;

    const blockProps = useBlockProps({
        className: `image-block image-block_align-${align}`,
    });

    const handleImageSelection = value => {
        setAttributes({
            imageId: value.id,
            altText: value.alt,
            caption: value.caption,
        });
    };

    const removeImage = () => setAttributes({ imageId: null });

    const handleFocalPointChange = value => {
        setAttributes({ focalPoint: value });
    };

    return (
        <>
            <InspectorControls>
                <PanelBody title={__('Image Settings')}>
                    <TextControl
                        label={__('Alt Text')}
                        value={altText}
                        onChange={(newAltText) => setAttributes({ altText: newAltText })}
                    />
                    <TextControl
                        label={__('Caption')}
                        value={caption}
                        onChange={(newCaption) => setAttributes({ caption: newCaption })}
                    />
                    <SelectControl
                        label={__('Image Width')}
                        value={align}
                        options={[
                            { label: 'Full Width', value: 'full' },
                            { label: 'right', value: 'right' },
                            { label: 'left', value: 'left' },
                        ]}
                        onChange={(newWidth) => setAttributes({ align: newWidth })}
                    />
                </PanelBody>
            </InspectorControls>
            <BlockControls>
                <MediaToolbar id={imageId} onSelect={handleImageSelection} isOptional={true} onRemove={removeImage} />
            </BlockControls>
            <div {...blockProps}>
                <Image id={imageId} size="large" onSelect={handleImageSelection} className="example-image" focalPoint={focalPoint} onChangeFocalPoint={handleFocalPointChange} />
                { caption && <div className="image__caption">{caption}</div>}
            </div>
        </>
    );
}
