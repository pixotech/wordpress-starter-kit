import {useBlockProps} from '@wordpress/block-editor';
import {Link, InlineIconPicker} from '@10up/block-components';

const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {linkText, linkUrl, opensInNewTab, icon} = attributes;

    const blockProps = useBlockProps(
        {
            className: 'link-with-icon',
        }
    );

    const handleIconChange = value => setAttributes(
        {
            icon: {name: value.name, iconSet: 'example/theme'},
        }
    );

    const handleTextChange = value => setAttributes({linkText: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
        console.log(value);
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });

    return (
        <li {...blockProps}>
            <div className="link-with-icon__link">
                <div className="link-with-icon__icon">
                    <div className="color-circle-icon">
                        <span className="color-circle-icon">
                            <InlineIconPicker value={icon} onChange={handleIconChange} className="icon-preview"/>
                        </span>
                    </div>
                </div>
                <Link
                    value={linkText}
                    url={linkUrl}
                    opensInNewTab={opensInNewTab}
                    onTextChange={handleTextChange}
                    onLinkChange={handleLinkChange}
                    onLinkRemove={handleLinkRemove}
                    className='link-with-icon__link link-with-icon__text'
                    placeholder='Enter Link Text here...'
                />
            </div>
        </li>
    );
};


export default Edit;
