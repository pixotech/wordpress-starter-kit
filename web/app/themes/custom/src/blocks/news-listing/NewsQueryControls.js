import {QueryControls} from '@wordpress/components';
import {withState} from '@wordpress/compose';

let customQuery = ({orderBy, order, category, categories, numberOfItems, setState, onTaxonomyChange, label}) => {
    return (
        <QueryControls
            {...{order}}
            label={label}
            categoriesList={categories}
            selectedCategoryId={category}
            onCategoryChange={(category) => {
                setState({category})
                onTaxonomyChange(category);
            }}
        />
    );
};
const NewsQueryControls = withState({
    orderBy: 'title',
    order: 'asc',
    numberOfItems: 10,

})(customQuery);


export default NewsQueryControls;
