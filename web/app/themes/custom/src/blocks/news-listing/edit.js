import {__} from '@wordpress/i18n';
import {
    useBlockProps,
    InspectorControls,
} from '@wordpress/block-editor';
import {Spinner, PanelBody, ToggleControl} from '@wordpress/components';
import {useAllTerms} from '@10up/block-components';
import NewsQueryControls from "./NewsQueryControls";

const Edit = (props) => {
    const {attributes, setAttributes} = props;
    const {filterByGlobalTaxonomy, filterByLocalTaxonomy, globalTaxonomy, localTaxonomy} = attributes;
    const handleGlobalTaxonomyChange = value => {
        setAttributes({globalTaxonomy: value});
    }
    const [globalOptions, hasResolvedGlobalOptions] = useAllTerms('units_and_areas');
    const [localOptions, hasResolvedLocalOptions] = useAllTerms('news_types');

    if (hasResolvedGlobalOptions && globalOptions.length > 0) {
      setAttributes({globalOptions: globalOptions});
    }

    if (hasResolvedLocalOptions && localOptions.length > 0) {
       setAttributes({localOptions: localOptions});
    }

    const blockProps = useBlockProps({
        className: 'news-listing',
    });

    return (
        <>
            <div {...blockProps}>
                <p>Filter options</p>
                {filterByGlobalTaxonomy && !hasResolvedGlobalOptions && <Spinner/>}
                {filterByGlobalTaxonomy && hasResolvedGlobalOptions && globalOptions.length === 0 && <p>No global taxonomy found.</p>}
                {filterByGlobalTaxonomy && hasResolvedGlobalOptions && globalOptions.length > 0 && (
                    <NewsQueryControls
                        categories={globalOptions}
                        category={globalTaxonomy}
                        onTaxonomyChange={handleGlobalTaxonomyChange}
                    />
                )}
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Filter by taxonomy', 'custom')}
                        checked={filterByGlobalTaxonomy}
                        onChange={() => setAttributes({filterByGlobalTaxonomy: !filterByGlobalTaxonomy})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};

export default Edit;
