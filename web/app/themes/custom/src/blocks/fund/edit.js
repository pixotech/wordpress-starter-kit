import { __ } from '@wordpress/i18n';
import { RichText, useBlockProps } from '@wordpress/block-editor';

const Edit = ( props ) => {
    const { attributes, setAttributes } = props;

    const { name, description, id } = attributes;

    const blockProps = useBlockProps(
        {
            className: 'fund',
        }
    );

    const handleNameChange = value => setAttributes({name: value});
    const handleDescriptionChange = value => setAttributes({description: value});
    const handleIdChange = value => setAttributes({id: value});

    return (
        <li {...blockProps}>
            <div className="fund__container">
                <RichText
                    tagName="p"
                    placeholder={ __(
                        'Fund name',
                        'custom'
                    ) }
                    value={name}
                    onChange={handleNameChange}
                    className="fund__name"
                    allowedFormats={ [] }
                />
                <RichText
                    tagName="p"
                    placeholder={ __(
                        'Fund description',
                        'custom'
                    ) }
                    value={description}
                    onChange={handleDescriptionChange}
                    className="fund__description"
                    allowedFormats={ [] }
                />
                <RichText
                    tagName="p"
                    placeholder={ __(
                        'Fund id',
                        'custom'
                    ) }
                    value={id}
                    onChange={handleIdChange}
                    className="fund__id"
                    allowedFormats={ [] }
                />
            </div>
            <div className="fund__button-container">
                <button className="fund__button standard-button">Give</button>
            </div>
        </li>

    );
};


export default Edit;
