<?php

$context = Timber::context();

// Store block values.
$context['block'] = $block;

// Store field values.
$context['fields'] = get_fields();

// Store $is_preview value.
$context['is_preview'] = $is_preview;

//    dd($context);
// Render the block.
Timber::render('patterns/blocks/link-card/link-card.twig', $context);
