import {Link} from '@10up/block-components';
import {Icon} from '@wordpress/components';

const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {linkText, linkUrl, opensInNewTab, linkType} = attributes;

    const handleTextChange = value => setAttributes({linkText: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText,
            linkType: "cloud"
        });
    }

    const url = window.location;
    const baseDomain = url.hostname;

    function checkIfUrlContainsExtension() {
        const fileExtensions = ['.pdf', '.jpeg', '.jpg', '.png', '.gif', '.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.zip', '.rar', '.7z', '.mp3', '.mp4', '.mov', '.avi', '.wmv', '.txt', '.csv', '.tsv', '.xml', '.json', '.svg', '.eps', '.ai', '.psd', '.indd', '.dwg', '.sketch', '.xd', '.fig', '.webp', '.heic', '.tif', '.tiff', '.bmp', '.ico', '.odt', '.ods', '.odp', '.odg', '.odf', '.pages', '.numbers', '.key', '.3gp', '.aac', '.flac', '.m4a', '.mp3', '.ogg', '.wav', '.wma', '.gifv', '.m4v', '.mkv', '.mov', '.mp4', '.mpeg', '.mpg', '.webm', '.avi', '.wmv', '.doc', '.docx', '.odt', '.pdf', '.ppt', '.pptx', '.xls', '.xlsx', '.zip', '.rar', '.7z', '.gz', '.tar', '.bz2', '.dmg', '.iso', '.jar', '.apk', '.exe', '.msi', '.cab', '.deb', '.rpm', '.css', '.html', '.js', '.php', '.rss', '.json', '.svg', '.ttf', '.eot', '.otf', '.woff', '.woff2', '.swf', '.flv', '.ico', '.cur', '.tif', '.tiff', '.bmp', '.jpg', '.jpeg', '.gif', '.png', '.webp', '.svg', '.eps', '.ai', '.psd', '.indd', '.dwg', '.sketch', '.xd', '.fig', '.webp', '.heic', '.tif', '.tiff', '.bmp', '.ico', '.odt', '.ods', '.odp', '.odg', '.odf', '.pages', '.numbers', '.key', '.3gp', '.aac', '.flac', '.m4a', '.mp3', '.ogg', '.wav', '.wma', '.gifv', '.m4v', '.mkv', '.mov'];
        let isFile = false;

        fileExtensions.forEach((extension) => {
            if (linkUrl && linkUrl.includes(extension)) {
                isFile = true;
            }
        });
        return isFile;
    }

    let isFile= checkIfUrlContainsExtension();


    const isExternal = linkUrl && !linkUrl.includes(baseDomain);

    let icon = {
        name: 'internal',
        iconSet: 'example/theme'
    }

    if(isFile) {
        setAttributes({linkType: 'file'});
        icon.name = 'archive';
    }
    else if(isExternal) {
        setAttributes({linkType: 'external'});
        icon.name = 'star-filled';
    }
    else {
        setAttributes({linkType: 'internal'});
        icon.name = 'cloud';
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });

    return (
        <li className="list-link__container">
            <Link
                value={linkText}
                url={linkUrl}
                opensInNewTab={opensInNewTab}
                onTextChange={handleTextChange}
                onLinkChange={handleLinkChange}
                onLinkRemove={handleLinkRemove}
                className='list-link'
                placeholder='Enter Link Text here...'
            />
            <span className="list-link__icon"><Icon icon={icon.name}/></span>
        </li>
    );
};


export default Edit;
