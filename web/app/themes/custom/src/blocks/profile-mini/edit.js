import {__} from '@wordpress/i18n';
import {RichText, InspectorControls, useBlockProps} from '@wordpress/block-editor';
import {PanelBody, ToggleControl} from '@wordpress/components';
import {Link, useAllTerms, ContentPicker} from "@10up/block-components";

const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {title, linkText, linkUrl, showTitle, showCTALink, profiles} = attributes;

    const blockProps = useBlockProps({
        className: 'profile-mini',
    });

    const [globalOptions, hasResolvedGlobalOptions] = useAllTerms('units_and_areas');
    const [localOptions, hasResolvedLocalOptions] = useAllTerms('news_types');

    if (hasResolvedGlobalOptions && globalOptions.length > 0) {
        setAttributes({globalOptions: globalOptions});
    }

    if (hasResolvedLocalOptions && localOptions.length > 0) {
        setAttributes({localOptions: localOptions});
    }

    const handleTextChange = value => setAttributes({linkText: value});
    const handleProfilesChange = value => setAttributes({profiles: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });


    return (
        <>
            <div {...blockProps}>
                {showTitle && (
                    <RichText
                        tagName="h2"
                        placeholder={__(
                            'The title is required for screen readers, but can be hidden from view if you like.',
                            'custom'
                        )}
                        value={title}
                        onChange={(title) => setAttributes({title})}
                        className="profile-mini__title"
                        allowedFormats={[]}
                    />
                )}
                <ContentPicker
                    onPickChange={ handleProfilesChange }
                    mode="post"
                    label={ "Please select a profile:" }
                    contentTypes={ [ 'profile' ] }
                    maxContentItems={ 100 }
                    isOrderable={ true }
                    content={ profiles }
                />
                {showCTALink && (
                    <div className="news-mini__cta-container">
                        <Link
                            value={linkText}
                            url={linkUrl}
                            onTextChange={handleTextChange}
                            onLinkChange={handleLinkChange}
                            onLinkRemove={handleLinkRemove}
                            className='standard-button'
                            placeholder='A very short (2-3 words) verb-driven phrase. Use a clear and specific verb like "Apply," "Explore," or "Connect."'
                        />
                    </div>
                )}
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show title', 'custom')}
                        checked={showTitle}
                        onChange={() => setAttributes({showTitle: !showTitle})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'gutenberg-lessons',
                        )}
                    />
                    <ToggleControl
                        label={__('Show CTA button', 'custom')}
                        checked={showCTALink}
                        onChange={() => setAttributes({showCTALink: !showCTALink})}
                        help={__(
                            'Enable call to action button on block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};

export default Edit;
