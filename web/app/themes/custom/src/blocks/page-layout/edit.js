import {__} from '@wordpress/i18n';
import {RichText, useBlockProps, InspectorControls, BlockControls} from '@wordpress/block-editor';
// import {Image, Link, MediaToolbar} from "@10up/block-components";
import {TextControl, PanelBody, ToggleControl} from '@wordpress/components';
import { useEntityProp } from '@wordpress/core-data';

export default function Edit(props) {
    const {attributes, setAttributes} = props;

    const {layout} = attributes;

    const handleLayoutChange = value => {
        setAttributes({
            layout: value
        });
        console.log(value);
        console.log(pageLayoutValue);
    }

    const [ meta, setMeta ] = useEntityProp( 'postType', 'page', 'meta' );

    const pageLayoutValue = meta[ 'page_layout' ];

    let isSaving = false;
    const updateMetaValue = ( newValue ) => {
        // only run once during period of saving


        // if(!isSaving) {
            isSaving = true;
            setMeta( { ...meta, page_layout: newValue } );
            console.log('updateMetaValue-' + newValue);
            isSaving = false;
        // }
    };

    const blockProps = useBlockProps(
        {
            className: 'page-layout',
        }
    );


    // On save (or load) set the metabox value for page_layout to the value of the block attribute
    wp.data.subscribe(() => {
        if (wp.data.select('core/editor').isSavingPost()) {
            // WIP: This function causes an infinite loop
            // updateMetaValue('layout');
            // console.log('saving-' + layout);
        }
    });

    return (
        <>
            <div {...blockProps}>

                <div className="page-layout__wrapper">
                    <h1>Page layout</h1>
                    <TextControl
                        label={__('Page layout', 'custom')}
                        value={layout}
                        onChange={handleLayoutChange}
                        className='page-layout__layout'
                    />
                    <h1>Page layout</h1>
                    <RichText
                        tagName="p"
                        value={layout}
                        onChange={handleLayoutChange}
                        className='page-layout__layout'
                    />
                </div>
            </div>
        </>
    );
};
