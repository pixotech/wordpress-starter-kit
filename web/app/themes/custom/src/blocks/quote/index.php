<?php

use Timber\Image;
use Timber\Timber;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 */

function quote_block_init() {
    register_block_type(
        __DIR__,
        array(
            'render_callback' => 'quote_block_render_callback',
        )
    );
}
add_action( 'init', 'quote_block_init' );


/**
 * This function is called when the block is being rendered on the front end of the site
 *
 * @param array    $attributes     The array of attributes for this block.
 * @param string   $content        Rendered block output. ie. <InnerBlocks.Content />.
 * @param WP_Block $block_instance The instance of the WP_Block class that represents the block being rendered.
 */
function quote_block_render_callback( $attributes, $content, $block_instance ) {

    ob_start();

    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;

    // Store field values.
    $context['fields'] = $attributes;
    $context['fields']['image']['url'] = new Image($attributes['imageId']);

    if($context['fields']['showCTALink']) {
        $context['fields']['linkText'] = $attributes['linkText'];
        $context['fields']['linkUrl'] = $attributes['linkUrl'];
    }

    $context['content'] = $content;

    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/quote/quote.twig', $context);

    return ob_get_clean();
}
