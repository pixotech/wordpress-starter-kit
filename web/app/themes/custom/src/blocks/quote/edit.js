import {__} from '@wordpress/i18n';
import {RichText, useBlockProps, InspectorControls, BlockControls} from '@wordpress/block-editor';
import {Image, Link, MediaToolbar} from "@10up/block-components";
import {PanelBody, ToggleControl} from '@wordpress/components';

export default function Edit(props) {
    const {attributes, setAttributes} = props;

    const {quoteText, attribution, imageId, focalPoint, linkText, linkUrl, opensInNewTab, showCTALink} = attributes;

    const blockProps = useBlockProps(
        {
            className: 'quote',
        }
    );

    const handleQuoteTextChange = value => setAttributes({quoteText: value});
    const handleAttributionChange = value => setAttributes({attribution: value});
    const handleTextChange = value => setAttributes({linkText: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });
    const handleImageSelect = (image) => {
        setAttributes({imageId: image.id});
    }

    const handleImageRemove = () => {
        setAttributes({imageId: null})
    }

    const handleFocalPointChange = (value) => {
        setAttributes({focalPoint: value});
    }

    return (
        <>
            <div {...blockProps}>
                <BlockControls>
                    <MediaToolbar
                        isOptional
                        id={imageId}
                        onSelect={handleImageSelect}
                        onRemove={handleImageRemove}
                    />
                </BlockControls>
                <Image
                    id={imageId}
                    className="quote__image-wrapper"
                    size="full"
                    onSelect={handleImageSelect}
                    focalPoint={focalPoint}
                    onChangeFocalPoint={handleFocalPointChange}
                    labels={{
                        title: 'A clear photo of the person quoted. Portrait dimensions work best.',
                        instructions: 'Upload a media file or pick one from your media library.'
                    }}
                />
                <div className="quote__content-container">
                    <RichText
                        tagName="p"
                        placeholder={__(
                            'The exact words of the quote, along with any necessary punctuation. Keep the quote concise and impactful, with a maximum of 1-2 sentences.',
                            'custom'
                        )}
                        value={quoteText}
                        onChange={handleQuoteTextChange}
                        className="quote__quote"
                        allowedFormats={['core/bold', 'core/italic']}
                    />
                    <RichText
                        tagName="p"
                        placeholder={__(
                            'The name of the person quoted. You can also add a short description of the person here, ex: "Jane Doe, BADAS 2004".',
                            'custom')}
                        className="quote__attribution"
                        value={attribution}
                        onChange={handleAttributionChange}
                        allowedFormats={[]}
                    />
                    {showCTALink && (
                        <Link
                            value={linkText}
                            url={linkUrl}
                            opensInNewTab={opensInNewTab}
                            onTextChange={handleTextChange}
                            onLinkChange={handleLinkChange}
                            onLinkRemove={handleLinkRemove}
                            className='quote__button standard-button'
                            placeholder='A very short (2-3 words) verb-driven phrase. Use a clear and specific verb like "Apply," "Explore," or "Connect."'
                        />)}
                </div>
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show CTA button', 'custom')}
                        checked={showCTALink}
                        onChange={() => setAttributes({showCTALink: !showCTALink})}
                        help={__(
                            'Enable call to action button on block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};
