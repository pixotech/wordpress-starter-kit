import { __ } from '@wordpress/i18n';
import { RichText, useBlockProps, useInnerBlocksProps} from '@wordpress/block-editor';

export default function Edit(props) {
    const { attributes, setAttributes } = props;

    const { heading, description } = attributes;

    const blockProps = useBlockProps( {
        className: 'link-cards',
    } );

    const innerBlocksProps = useInnerBlocksProps(
        {
            className: 'link-cards__cards',
        },
        {
            allowedBlocks: [
                'custom/link-card',
            ],
            template: [
                [ 'custom/link-card' ],
            ],
        });

	return (
		<div {...blockProps }>
            <div className="link-cards__introduction">
                <RichText
                    tagName="h2"
                    placeholder={ __(
                        'A short (about 2-6 words) title for your link, e.g. the name of a program or a student organization.',
                        'custom'
                    ) }
                    value={heading}
                    onChange={(heading) => setAttributes({heading})}
                    className="link-cards__heading"
                    allowedFormats={ [ 'core/bold', 'core/italic' ] }
                />
                <RichText
                    tagName="p"
                    placeholder={ __(
                        'A short (about 10-20 words) summary or description of the subject of your link, e.g. the benefits of a program or student organization.',
                        'custom'
                    ) }
                    value={description}
                    onChange={(description) => setAttributes({description})}
                    className="link-cards__description"
                    allowedFormats={ [ 'core/bold', 'core/italic' ] }
                />
            </div>
            <ul {...innerBlocksProps} />
		</div>
	);
}
