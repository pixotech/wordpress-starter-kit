import {__} from '@wordpress/i18n';
import {RichText, BlockControls, InspectorControls, useBlockProps} from '@wordpress/block-editor';
import {Link, Image, MediaToolbar} from '@10up/block-components';
import {PanelBody, ToggleControl} from '@wordpress/components';

const Edit = (props) => {
    const {attributes, setAttributes} = props;

    const {
        heading,
        description,
        linkText,
        linkUrl,
        opensInNewTab,
        imageId,
        focalPoint,
        showImage,
        showCTALink
    } = attributes;

    const blockProps = useBlockProps(
        {
            className: 'topic-block',
        }
    );

    const handleHeadingChange = value => setAttributes({heading: value});
    const handleTextChange = value => setAttributes({linkText: value});
    const handleDescriptionChange = value => setAttributes({description: value});
    const handleLinkChange = value => {
        setAttributes({
            linkUrl: value?.url,
            opensInNewTab: value?.opensInNewTab,
            linkText: value?.title ?? linkText
        });
        console.log(value);
    }
    const handleLinkRemove = () => setAttributes({
        linkUrl: null,
        opensInNewTab: null,
    });
    const handleImageSelect = (image) => {
        setAttributes({imageId: image.id});
        console.log(image);
    }

    const handleImageRemove = () => {
        setAttributes({imageId: null})
    }

    const handleFocalPointChange = (value) => {
        setAttributes({focalPoint: value});
        console.log(value);
    }

    return (
        <>
            <div {...blockProps}>
                <div className={showImage ? ' topic-block__content-container topic-block__content-container--with-image' : 'topic-block__content-container'}>
                    <div className="topic-block__copy-wrapper">
                        <RichText
                            tagName="h3"
                            placeholder={__(
                                'A short and descriptive header that summarizes the main message or topic.',
                                'custom'
                            )}
                            onChange={handleHeadingChange}
                            value={heading}
                            className="topic-block__heading"
                        />
                        <RichText
                            tagName="p"
                            placeholder={__(
                                'Concise (3-4 sentences at most) additional information about the topic. If needed, add a call to action to link to more details.',
                                'gutenberg-examples'
                            )}
                            value={description}
                            onChange={handleDescriptionChange}
                            className="topic-block__description"
                            allowedFormats={['core/bold', 'core/italic']}
                        />
                        {showCTALink && (
                            <Link
                                value={linkText}
                                url={linkUrl}
                                opensInNewTab={opensInNewTab}
                                onTextChange={handleTextChange}
                                onLinkChange={handleLinkChange}
                                onLinkRemove={handleLinkRemove}
                                className='topic-block__button standard-button'
                                placeholder='A very short (2-3 words) verb-driven phrase. Use a clear and specific verb like "Apply," "Explore," or "Connect."'
                            />)}
                    </div>
                    {showImage && (
                        <div className="topic-block__image-wrapper">
                            <BlockControls>
                                <MediaToolbar
                                    isOptional
                                    id={imageId}
                                    onSelect={handleImageSelect}
                                    onRemove={handleImageRemove}
                                />
                            </BlockControls>
                            <Image
                                id={imageId}
                                className="topic-block__image"
                                size="full"
                                onSelect={handleImageSelect}
                                focalPoint={focalPoint}
                                onChangeFocalPoint={handleFocalPointChange}
                                labels={{
                                    title: 'Square or portrait (vertical) images work best. Choose the focal point of the image for best results.',
                                    instructions: 'Upload a media file or pick one from your media library.'
                                }}
                            />
                        </div>
                    )}
                </div>
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show image', 'custom')}
                        checked={showImage}
                        onChange={() => setAttributes({showImage: !showImage})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                    <ToggleControl
                        label={__('Show CTA button', 'gutenberg-lessons')}
                        checked={showCTALink}
                        onChange={() => setAttributes({showCTALink: !showCTALink})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'gutenberg-lessons',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};


export default Edit;
