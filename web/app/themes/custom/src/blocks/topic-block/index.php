<?php

use Timber\Timber;
use Timber\Image;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 */

function topic_block_block_init() {
    register_block_type(
        __DIR__,
        array(
            'render_callback' => 'topic_block_block_render_callback',
        )
    );
}
add_action( 'init', 'topic_block_block_init' );


/**
 * This function is called when the block is being rendered on the front end of the site
 *
 * @param array    $attributes     The array of attributes for this block.
 * @param string   $content        Rendered block output. ie. <InnerBlocks.Content />.
 * @param WP_Block $block_instance The instance of the WP_Block class that represents the block being rendered.
 */

function topic_block_block_render_callback( $attributes, $content, $block_instance ) {

    ob_start();

    $context = Timber::context();
    $context['block'] = $block_instance;

    // Store field values.
    $context['fields'] = $attributes;
    $context['fields']['heading'] = $attributes['heading'];
    $context['fields']['description'] = $attributes['description'];
    if($context['fields']['showCTALink']) {
        $context['fields']['linkText'] = $attributes['linkText'];
        $context['fields']['linkUrl'] = $attributes['linkUrl'];
    }
    $context['fields']['showImage'] = $attributes['showImage'];
    if($context['fields']['showImage']) {
        $context['fields']['image']['url'] = new Image($attributes['imageId']);
    }

    $context['content'] = $content;

    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/topic-block/topic-block.twig', $context);

    return ob_get_clean();
}
