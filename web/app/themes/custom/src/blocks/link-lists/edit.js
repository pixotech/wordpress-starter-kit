import {__} from '@wordpress/i18n';
import {RichText, useBlockProps, useInnerBlocksProps, InspectorControls} from '@wordpress/block-editor';
import {PanelBody, ToggleControl} from '@wordpress/components';

export default function Edit(props) {
    const {attributes, setAttributes} = props;

    const {header, showDescription, description} = attributes;

    const blockProps = useBlockProps({
        className: 'link-lists',
    });

    const handleHeaderChange = value => setAttributes({header: value});

    const handleDescriptionChange = value => setAttributes({description: value});

    const innerBlocksProps = useInnerBlocksProps(
        {
            className: 'link-lists__container',
        },
        {
            allowedBlocks: [
                'custom/link-list',
            ],
            template: [
                ['custom/link-list'],
            ],
        });
    return (
        <>
            <div {...blockProps}>
                <div>
                    <RichText
                        tagName="p"
                        placeholder={__(
                            'Header',
                            'custom'
                        )}
                        value={header}
                        onChange={handleHeaderChange}
                        className="link-lists__header"
                        allowedFormats={['core/bold', 'core/italic']}
                    />
                    {showDescription && (
                        <RichText
                            tagName="p"
                            placeholder={__(
                                'A brief (10-20 words) summary of the subject of your lists.'
                            )}
                            value={description}
                            onChange={handleDescriptionChange}
                            className="link-lists__description"
                            allowedFormats={['core/bold', 'core/italic']}
                        />
                    )}
                </div>
                <div {...innerBlocksProps}></div>
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show description', 'custom')}
                        checked={showDescription}
                        onChange={() => setAttributes({showDescription: !showDescription})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};
