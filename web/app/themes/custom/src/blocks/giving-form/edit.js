import {__} from '@wordpress/i18n';
import {RichText, InspectorControls, useBlockProps, useInnerBlocksProps} from '@wordpress/block-editor';
import {PanelBody, ToggleControl} from '@wordpress/components';

export default function Edit(props) {
    const {attributes, setAttributes} = props;

    const {heading, linkText, linkUrl, showHeading} = attributes;

    const blockProps = useBlockProps({
        className: 'giving-form',
    });

    const innerBlocksProps = useInnerBlocksProps(
        {
            className: 'giving-form__funds',
        },
        {
            allowedBlocks: [
                'custom/fund',
            ],
            template: [
                ['custom/fund'],
            ],
        });

    return (
        <>
            <div {...blockProps}>
                {showHeading && (
                    <RichText
                        tagName="h2"
                        placeholder={__(
                            'Giving form heading',
                            'custom'
                        )}
                        value={heading}
                        onChange={(heading) => setAttributes({heading})}
                        className="giving-form__header"
                        allowedFormats={[]}
                    />
                )}
                <ul {...innerBlocksProps} />
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show heading', 'custom')}
                        checked={showHeading}
                        onChange={() => setAttributes({showHeading: !showHeading})}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'gutenberg-lessons',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
}
