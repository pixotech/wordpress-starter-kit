import {__} from '@wordpress/i18n';
import {
    BlockControls,
    RichText,
    useBlockProps,
    InspectorControls,
} from '@wordpress/block-editor';
import {PanelBody, ToggleControl} from '@wordpress/components';
import {Image, MediaToolbar} from '@10up/block-components';
import {useEntityProp} from '@wordpress/core-data';

const Edit = (props) => {
    const {attributes, setAttributes} = props;
    const {showPrefix} = attributes;

    const [meta, setMeta] = useEntityProp('postType', 'profile', 'meta');

    const metaFieldPrefix = meta['profile_meta_block_prefix'];
    const metaFieldFirstName = meta['profile_meta_block_first_name'];
    const metaFieldLastName = meta['profile_meta_block_last_name'];
    const metaFieldTitles = meta['profile_meta_block_titles'];
    const metaFieldFocalPoint = meta['profile_meta_block_focal_point'];
    const metaFieldFeaturedImage = meta['profile_meta_block_image_id'];
    const metaFieldEmail = meta['profile_meta_block_email'];
    const metaFieldPhoneNumber = meta['profile_meta_block_phone_number'];

    const updateMetaPrefix = (newValue) => {
        setMeta({...meta, profile_meta_block_prefix: newValue});
    };
    const updateMetaFirstNameValue = (newValue) => {
        setMeta({...meta, profile_meta_block_first_name: newValue});
        setTitle();
    };

    const updateMetaLastNameValue = (newValue) => {
        setMeta({...meta, profile_meta_block_last_name: newValue});
        setTitle();
    };

    const updateMetaTitlesValue = (newValue) => {
        setMeta({...meta, profile_meta_block_titles: newValue});
    };

    const updateMetaFocalPointValue = (newValue) => {
        setMeta({...meta, profile_meta_block_focal_point: JSON.stringify(newValue)});
    };

    const updateMetaFeaturedImageValue = (newValue) => {
        setMeta({...meta, profile_meta_block_image_id: newValue.id});
    };

    const removeMetaFeaturedImageValue = () => {
        setMeta({...meta, profile_meta_block_image_id: null});
    };

    const updateMetaEmailValue = (newValue) => {
        setMeta({...meta, profile_meta_block_email: newValue});
    };

    const updateMetaPhoneNumberValue = (newValue) => {
        setMeta({...meta, profile_meta_block_phone_number: newValue});
    };

    const setTitle = () => {
        let firstName = meta.profile_meta_block_first_name;
        let lastName = meta.profile_meta_block_last_name;

        let title = firstName + ' ' + lastName;

        wp.data.dispatch('core/editor').editPost({title: title});
    };

    const blockProps = useBlockProps({
        className: 'profile-post',
    });

    return (
        <>
            <div {...blockProps}>
                <BlockControls>
                    <MediaToolbar
                        isOptional
                        id={metaFieldFeaturedImage}
                        onSelect={updateMetaFeaturedImageValue}
                        onRemove={removeMetaFeaturedImageValue}
                    />
                </BlockControls>
                <Image
                    id={metaFieldFeaturedImage}
                    className="my-image"
                    size="full"
                    onSelect={updateMetaFeaturedImageValue}
                    focalPoint={metaFieldFocalPoint ? JSON.parse(metaFieldFocalPoint) : ''}
                    onChangeFocalPoint={updateMetaFocalPointValue}
                    labels={{
                        title: 'Select Featured Image',
                        instructions: 'Add an eye-catching image to add visual interest to the story. Choose a clear, compelling picture that represents the topic. Optimize the file size for faster loading.'
                    }}
                />
                {showPrefix && (
                    <RichText
                        tagName="p"
                        className="profile__prefix"
                        placeholder={__('Prefix(es) Ex: Dr.', 'custom')}
                        value={metaFieldPrefix}
                        onChange={updateMetaPrefix}
                    />
                )}
                <RichText
                    tagName="p"
                    className="profile__first-name"
                    placeholder={__('First name', 'custom')}
                    value={metaFieldFirstName}
                    onChange={updateMetaFirstNameValue}
                />
                <RichText
                    tagName="p"
                    className="profile__last-name"
                    placeholder={__('Last name', 'custom')}
                    value={metaFieldLastName}
                    onChange={updateMetaLastNameValue}
                />
                <RichText
                    tagName="p"
                    className="profile__titles"
                    placeholder={__('Separate multiple titles using "and" and/or commas.', 'custom')}
                    value={metaFieldTitles}
                    onChange={updateMetaTitlesValue}
                />
                <RichText
                    tagName="p"
                    className="profile__email"
                    placeholder={__('Email', 'custom')}
                    value={metaFieldEmail}
                    onChange={updateMetaEmailValue}
                />
                <RichText
                    tagName="p"
                    className="profile__phone-number"
                    placeholder={__('Phone number', 'custom')}
                    value={metaFieldPhoneNumber}
                    onChange={updateMetaPhoneNumberValue}
                />
            </div>
            <InspectorControls>
                <PanelBody>
                    <ToggleControl
                        label={__('Show prefix(es)', 'custom')}
                        checked={showPrefix}
                        onChange={() => setAttributes({ showPrefix: !showPrefix })}
                        help={__(
                            'This toggle lets you conditionally output other markup and attributes in the block.',
                            'custom',
                        )}
                    />
                </PanelBody>
            </InspectorControls>
        </>
    );
};

export default Edit;
