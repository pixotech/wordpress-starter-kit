<?php

add_filter( 'register_block_type_args', 'core_vimeo_block_type_args', 10, 3 );
function core_vimeo_block_type_args( $args, $name ) {
    if ( $name == 'core/embed' ) {
        $args['render_callback'] = 'modify_vimeo_block_render';
//        $args['variations'];
//        dd($args);
    }
    return $args;
}

function modify_vimeo_block_render($attributes, $content, $block_instance) {
    if ($attributes['providerNameSlug'] !== 'vimeo') {
        return $content;
    }
    ob_start();
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;

    // Store field values.
    $oembed = new WP_oEmbed;
    $oembed_data = $oembed->get_data($attributes['url']);
    $oembed_data->id = $oembed_data->video_id;
    $context['fields'] = $attributes;
    $context['content'] = $content;
    $context['fields']['oembed'] = $oembed_data;

    // get html from inside the <figure> tag



    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/vimeo/vimeo.twig', $context);

    return ob_get_clean();
}


