<?php

add_filter( 'register_block_type_args', 'core_youtube_block_type_args', 10, 3 );
function core_youtube_block_type_args( $args, $name ) {
    if ( $name == 'core/embed' ) {
        $args['render_callback'] = 'modify_youtube_block_render';
//        $args['variations'];
//        dd($args);
    }
    return $args;
}

function modify_youtube_block_render($attributes, $content, $block_instance) {
    if ($attributes['providerNameSlug'] !== 'youtube') {
        return $content;
    }
    ob_start();
    $context = Timber::context();

    // Store block values.
    $context['block'] = $block_instance;
//    dd($block_instance);
    // Store field values.
    $oembed = new WP_oEmbed;
    $oembed_data = $oembed->get_data($attributes['url']);
    $oembed_data->id = extractVideoIdFromYoutubeThumbnailUrl($oembed_data->thumbnail_url);
    $context['fields'] = $attributes;
//    $context['fields']['oembed'] = get_oembed_response_data_for_url($attributes['url'], array());
    $context['fields']['oembed'] = $oembed_data;
    $context['content'] = $content;


    // Render the block.
    Timber::render(get_template_directory() .'/patterns/blocks/youtube/youtube.twig', $context);

    return ob_get_clean();
}

function extractVideoIdFromYoutubeThumbnailUrl($url)
{
    // https://i.ytimg.com/vi/3cynqSLcV7o/hqdefault.jpg
    $urlParts = explode('/', $url);
    return $urlParts[count($urlParts) - 2];

}
