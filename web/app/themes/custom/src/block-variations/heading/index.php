<?php

function core_heading_block_type_args($args, $name)
{
    if ($name == 'core/heading') {
        $args['supports'] = [];
    }
    return $args;
}

add_filter('register_block_type_args', 'core_heading_block_type_args', 10, 3);
