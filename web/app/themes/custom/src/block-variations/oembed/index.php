<?php

namespace Custom\BlockVariations;
use DOMDocument;
use Timber\Timber;
use WP_oEmbed;

class OembedBlock
{
    public $providers = [
        'youtube',
        'vimeo',
    ];

    public function __construct()
    {
        add_filter('register_block_type_args', [$this, 'modifyCoreOembedBlockTypeArgs'], 10, 3);
    }

    public function modifyCoreOembedBlockTypeArgs($args, $name)
    {
        if ($name == 'core/embed') {
            $args['render_callback'] = [$this, 'modifyOembedBlockRender'];
        }
        return $args;
    }

    public function modifyOembedBlockRender($attributes, $content, $block_instance)
    {
        $blockArray = [
            'block' => $block_instance,
            'content' => $content,
            'fields' => $attributes,
        ];
        if (!in_array($attributes['providerNameSlug'], $this->providers)) {
            return $content;
        }
        if ($attributes['providerNameSlug'] === 'youtube') {
            return $this->modifyYoutubeBlockRender($blockArray);
        }
        if ($attributes['providerNameSlug'] === 'vimeo') {
            return $this->modifyVimeoBlockRender($blockArray);
        }
        return $content;
    }

    public function modifyYoutubeBlockRender($blockArray)
    {
        ob_start();
        $context = Timber::context();
        $context = self::getYoutubeViewModel($context, $blockArray);
        Timber::render(get_template_directory() . '/patterns/blocks/youtube/youtube.twig', $context);
        return ob_get_clean();
    }

    public function getYoutubeViewModel($context, $blockArray)
    {
        $context['block'] = $blockArray['block'];
        $oembed_data = self::getOembedData($blockArray['fields']['url']);
        $oembed_data->id = self::extractVideoIdFromYoutubeThumbnailUrl($oembed_data->thumbnail_url);
        $context['fields'] = $blockArray['fields'];
        $context['fields']['oembed'] = $oembed_data;
        $context['content'] = $blockArray['content'];
        $context['fields']['caption'] = self::extractCaptionTextFromOembed($blockArray['content']);

        return $context;
    }

    public function modifyVimeoBlockRender($blockArray)
    {
        ob_start();
        $context = Timber::context();
        $context = self::getVimeoViewmodel($context, $blockArray);
        Timber::render(get_template_directory() .'/patterns/blocks/vimeo/vimeo.twig', $context);
        return ob_get_clean();
    }

    public function getVimeoViewmodel($context, $blockArray)
    {

        $context['block'] = $blockArray['block'];

        $context['content'] = $blockArray['content'];

        // Store field values.
        $oembed_data = self::getOembedData($blockArray['fields']['url']);
        $oembed_data->id = $oembed_data->video_id;
        $context['fields'] = $blockArray['fields'];
        $context['fields']['oembed'] = $oembed_data;
        $context['fields']['caption'] = self::extractCaptionTextFromOembed($blockArray['content']);

        return $context;
    }

    public function getOembedData($url) {
        $oembed = new WP_oEmbed;
        $oembedData = $oembed->get_data($url);

        if($oembedData === false) {
            $oembed->isValid = false;
            $oembed->errorMessage = "Invalid URL";
        }
        return $oembedData;
    }

    public function extractVideoIdFromYoutubeThumbnailUrl($url) : string
    {
        if (empty($url)) {
            return "__missingThumbnailUrl__";
        }
        $urlParts = explode('/', $url);
        $videoId = $urlParts[count($urlParts) - 2];

        return $videoId;
    }

    public function extractCaptionTextFromOembed($html)
    {
        $dom = new DOMDocument();
        $dom->loadHTML($html, LIBXML_NOERROR);
        $figcaption = $dom->getElementsByTagName('figcaption')->item(0);
        $innerHtml = $dom->saveHTML($figcaption);

        return $innerHtml;
    }
}

new OembedBlock();
