<?php

function core_paragraph_block_type_args($args, $name)
{
    if ($name == 'core/paragraph') {
        $args['supports'] = [];
    }
    return $args;
}

add_filter('register_block_type_args', 'core_paragraph_block_type_args', 10, 3);



