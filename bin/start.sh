#!/bin/bash

cd `git rev-parse --show-toplevel`

echo "---- Stopping all running docker containers ----"
docker stop $(docker ps -a -q)

echo "---- Running sync-local script ----"
source ./bin/sync-local.sh

echo "---- READY! ----"
echo "----------------"
echo "-- Login at"
echo "-- ${WP_HOME}/wp/wp-admin"
echo "-- Username: localuser"
echo "-- Password: localuser"
echo "-- Monitor email at:"
echo "-- http://mailhog.${PROJECT_BASE_URL}"
echo "-- PHPMyAdmin:"
echo "-- http://pma.${PROJECT_BASE_URL}"
echo "----------------"

echo "---- Use 'docker-compose stop' to stop! ----"
