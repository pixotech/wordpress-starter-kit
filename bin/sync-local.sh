#!/bin/bash

cd `git rev-parse --show-toplevel`

source .env

echo "---- Pulling from git ----"
git pull --prune

cd web/app/themes/custom
echo "---- Installing NPM packages and building assets ----"
npm install
npm run build
cd `git rev-parse --show-toplevel`

echo "---- Starting docker containers ----"
docker-compose up -d

echo "---- Installing packages via composer ----"
docker-compose exec php composer install
docker-compose exec --workdir=/var/www/html/web/app/themes/custom php composer install

echo "---- Updating Wordpress database ----"
docker-compose exec php wp core update-db
echo "---- Flushing rewrite rules (permalinks and other URL stuff) ----"
docker-compose exec php wp rewrite flush
