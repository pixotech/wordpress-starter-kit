#!/bin/bash

cd `git rev-parse --show-toplevel`

source .env.test

git pull --prune

cd web/app/themes/custom
npm install
npm run blocks:build
cd `git rev-parse --show-toplevel`

docker-compose up -d

echo "Installing packages via composer"
docker-compose exec php-test export COMPOSER_AUTH='{"http-basic": {"connect.advancedcustomfields.com": {"username": "$ACF_PRO_KEY", "password": "http://starter-kit.localhost"}}}'
docker-compose exec php-test composer install
docker-compose exec --workdir=/var/www/html/web/app/themes/custom php-test composer install
docker-compose exec php-test wp core update-db
docker-compose exec php-test wp rewrite flush
docker-compose exec php-test wp plugin install wordpress-importer --activate
docker-compose exec php-test wp post delete $(wp post list --format=ids) --force
docker-compose exec php-test wp import bin/testing.xml --authors=skip
