#!/bin/bash

read -e -p "Before you begin make sure you have a database dump in /database-dump.sql" check

DOCKER='docker-compose exec php'

cd `git rev-parse --show-toplevel`

echo "---- Clearing the database ----"
${DOCKER} wp db reset --yes

echo "---- Importing the database ----"
${DOCKER} wp db import database-dump.sql

# We don't need to rename the database URLs for a local site since the site uses the PROJECT_BASE_URL in the .env over the database URLs.
# If there are issues, make sure the correct PROJECT_BASE_URL is defined there, or potentially look into searching/replacing URLs in the database.

echo "---- DONE! ----"
