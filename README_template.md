# TODO: [project name]

TODO: [project description/introduction]

This was built using the [Pixo](https://pixotech.com) WordPress Starter Kit.

## Documentation

For more detailed documentation refer to the files in the [./docs/](/docs/) directory.

## Development stack

This project makes heavy use of the following projects to modernize the Wordpress development stack.

### [Bedrock](https://roots.io/bedrock/)

Bedrock is a project that repackages WordPress into a new directory structure and provides the following features.

* A separate `web` root directory
* Composer dependency management
* Environment configuration framework

Read the [documentation](https://roots.io/docs/bedrock/master/installation/#getting-started) for more information.

### [Lumberjack](https://lumberjack.rareloop.com/)

The [Lumberjack](https://lumberjack.rareloop.com/) is a theme framework that provides utilities and an organizational structure for coding custom functionality.
Some of that functionality includes:

* [Timber](https://upstatement.com/timber/) for Twig templating and MVC viewmodel and controller support.
* Post type registration
* Menu registration
* A query builder
* [and much more](https://docs.lumberjack.rareloop.com/)


### ACF Builder
[ACF Builder](https://github.com/StoutLogic/acf-builder) is a toolkit for building Advanced Custom Fields field group configurations
in your project. It uses a set if chain-able methods to rapidly build out your author UI as well as the ability to
create re-usable field patterns.

### Docker

This project includes a Docker Compose configuration configured specifically for WordPress local development. It includes
the following features.

* PHP-FPM container with support for current and past versions
* Nginx container pre-configured for WordPress
* MariaDB container for the database
* [Mailhog](https://github.com/mailhog/MailHog) container for email testing
* PHPMyAdmin container for database management
* [Traefik](https://github.com/traefik/traefik) container for routing traffic by hostname on your local environment

## Project setup

### Requirements

* [Docker](https://www.docker.com/)
* Bash/Zsh
* Node 22+ (see build tooling documentation in [./docs/build-tooling.md](./docs/build-tooling.md))

### Local setup

To set up your local environment run the setup script.

1. Run `bin/setup.sh`. This sets up the local Docker environment and Wordpress.

That is it! The setup script will tell you the url of your new site.

#### Importing a database dump

Use the importing script to import a database dump into the local environment. Before running it, make sure to copy the dump into the main directory and rename it to `database-dump.sql`.

`bin/import-database-dump.sh`

To add media uploads to the site, copy/paste the content of a live version of the site's `web/app/uploads/` directory into the same location in the local site.

## Workflow

Use the start script to sync your local environment and start the necessary docker containers.
* `bin/start.sh`

The start script runs sync-local within it, but you can run it by itself too. This will update your environment by updating or installing any new dependencies and database migrations.
* `bin/sync-local.sh`

## Front-end stack

* Webpack is used in the theme directory to compile and process Stylus for CSS and ES6 Javascript.
* Stylus for the CSS pre-processor.
* [Stylelint](https://stylelint.io/) enforces many theming best practices and code style. See docs at [./docs/stylelint.md](./docs/stylelint.md)

### Build & watch process

* `cd web/app/themes/custom`
* `npm run watch` to run the build and watch files
* `npm run dev` to run a development build
* `npm run build` to run a production build
* `npm run fractal-sync` to run Fractal locally. This watches files and is a great tool for doing front-end development
* `npm run fractal-build` to build a static Fractal website.

### Fractal

[Fractal](https://fractal.build) is a pattern library tool for generating a static website that includes your theme
template patterns along with annotations, and other useful meta data.

USAGE: Every Twig template that is created should include a `[template-name].config.js` configuration file along-side it.
It should represent all the variations of that template so that the library becomes a useful reference for the designs.





