# Wordpress Starter Kit project

This is a WordPress starter kit for [Pixo](https://pixotech.com) and can be used as a starting point for any new WordPress
project. It includes common features and functionality that we use in most projects and allows a developer to start
development within [just a few minutes](#markdown-header-project-setup).

For steps to take when using the Starter Kit for a new project, see [new project start checklist](docs/new-project-start-checklist.md).

This README file is for the purpose of using the Wordpress Starter Kit and should be replaced by the [README_template.md](README_template.md) when using this to start a new project.

## Setup

See [README_template.md](README_template.md) for local setup. Note that this setup should be done after the above checklist when starting a new project.

## Documentation

For more documentation refer to the files in the [docs/](docs/) directory.



